# gre tunnel between containers

Simple testbed with GRE tunnels between containers, to develop XDP code for r2 doing the
decap/encap/route function instead of the kernel's gre function.

Static routes in each container will later be replaced with cRPD doing BGP. But one step at a
time ...

```mermaid                                                                                             
graph LR
r1 ---|10.12.12.0/24| r2 ---|10.23.23.0/24| r3
```

Build and bring up the containers with

```
$ make 
```

check they are running

```
$ docker ps
gre$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
a774b2db5423        gre_r1              "/bin/sh /entrypoint…"   3 seconds ago       Up 2 seconds                            r1
8cbc3058db88        gre_r3              "/bin/sh /entrypoint…"   3 seconds ago       Up 2 seconds                            r3
01a715ce0c5b        gre_r2              "/bin/sh /entrypoint…"   3 seconds ago       Up 2 seconds                            r2
```

Then start a shell in r1, pinging the remote loopback IP of r2, while running a tcpdump in r2

```
$ docker exec -ti r1 ash
/ # ping 10.0.0.2
PING 10.0.0.2 (10.0.0.2): 56 data bytes
64 bytes from 10.0.0.2: seq=0 ttl=64 time=0.215 ms
64 bytes from 10.0.0.2: seq=1 ttl=64 time=0.111 ms
64 bytes from 10.0.0.2: seq=2 ttl=64 time=0.107 ms
^C
--- 10.0.0.2 ping statistics ---
3 packets transmitted, 3 packets received, 0% packet loss
round-trip min/avg/max = 0.107/0.144/0.215 ms
```

```
$ docker exec -ti r2 ash
/ # tcpdump -n -i eth0
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes
06:00:43.264513 IP6 fe80::dcdd:32ff:fe5d:456b.5353 > ff02::fb.5353: 0 [2q] PTR (QM)? _ipps._tcp.local. PTR (QM)? _ipp._tcp.local. (45)
06:00:46.198554 ARP, Request who-has 10.12.12.12 tell 10.12.12.11, length 28
06:00:46.198577 ARP, Reply 10.12.12.12 is-at 02:42:0a:0c:0c:0c, length 28
06:00:46.198606 IP 10.12.12.11 > 10.12.12.12: GREv0, length 88: IP 10.0.0.1 > 10.0.0.2: ICMP echo request, id 4608, seq 0, length 64
06:00:46.198650 IP 10.12.12.12 > 10.12.12.11: GREv0, length 88: IP 10.0.0.2 > 10.0.0.1: ICMP echo reply, id 4608, seq 0, length 64
06:00:47.073525 IP6 fe80::42:aff:fe0c:c0c > ff02::2: ICMP6, router solicitation, length 16
06:00:47.198819 IP 10.12.12.11 > 10.12.12.12: GREv0, length 88: IP 10.0.0.1 > 10.0.0.2: ICMP echo request, id 4608, seq 1, length 64
06:00:47.198849 IP 10.12.12.12 > 10.12.12.11: GREv0, length 88: IP 10.0.0.2 > 10.0.0.1: ICMP echo reply, id 4608, seq 1, length 64
06:00:48.198983 IP 10.12.12.11 > 10.12.12.12: GREv0, length 88: IP 10.0.0.1 > 10.0.0.2: ICMP echo request, id 4608, seq 2, length 64
06:00:48.199011 IP 10.12.12.12 > 10.12.12.11: GREv0, length 88: IP 10.0.0.2 > 10.0.0.1: ICMP echo reply, id 4608, seq 2, length 64
06:00:48.579075 IP 10.12.12.1.17500 > 10.12.12.255.17500: UDP, length 144
06:00:49.119334 IP6 10:12:12::1.5353 > ff02::fb.5353: 0 [2q] PTR (QM)? _ipps._tcp.local. PTR (QM)? _ipp._tcp.local. (45)
06:00:49.119429 IP 10.12.12.1.5353 > 224.0.0.251.5353: 0 [2q] PTR (QM)? _ipps._tcp.local. PTR (QM)? _ipp._tcp.local. (45)
06:00:49.121539 IP6 fe80::42:aff:fe0c:c0b > ff02::2: ICMP6, router solicitation, length 16
06:00:51.266301 IP6 fe80::dcdd:32ff:fe5d:456b.5353 > ff02::fb.5353: 0 [2q] PTR (QM)? _ipps._tcp.local. PTR (QM)? _ipp._tcp.local. (45)
06:00:51.425548 ARP, Request who-has 10.12.12.11 tell 10.12.12.12, length 28
06:00:51.425612 ARP, Reply 10.12.12.11 is-at 02:42:0a:0c:0c:0b, length 28
^C
17 packets captured
17 packets received by filter
0 packets dropped by kernel
```

## References

[example packet rewrite from the xdp_tutorial](https://github.com/atoonk/xdp-tutorial/blob/master/common/rewrite_helpers.h)



Example GRE packets

```
gre$ docker exec -ti r2 tcpdump -n -s 1500 -e -XX -i eth0 
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth0, link-type EN10MB (Ethernet), capture size 1500 bytes

07:52:04.963177 02:42:0a:0c:0c:0b > 02:42:0a:0c:0c:0c, ethertype IPv4 (0x0800), length 122: 10.12.12.11 > 10.12.12.12: GREv0, proto IPv4 (0x0800), length 88: 10.0.0.1 > 10.0.0.2: ICMP echo request, id 8704, seq 0, length 64
        0x0000:  0242 0a0c 0c0c 0242 0a0c 0c0b 0800 4500  .B.....B......E.
        0x0010:  006c e284 4000 ff2f 6caf 0a0c 0c0b 0a0c  .l..@../l.......
        0x0020:  0c0c 0000 0800 4500 0054 f2cf 4000 4001  ......E..T..@.@.
        0x0030:  33d7 0a00 0001 0a00 0002 0800 1d13 2200  3.............".
        0x0040:  0000 3e5e 7a8e 0000 0000 0000 0000 0000  ..>^z...........
        0x0050:  0000 0000 0000 0000 0000 0000 0000 0000  ................
        0x0060:  0000 0000 0000 0000 0000 0000 0000 0000  ................
        0x0070:  0000 0000 0000 0000 0000                 ..........

07:52:04.963214 02:42:0a:0c:0c:0c > 02:42:0a:0c:0c:0b, ethertype IPv4 (0x0800), length 122: 10.12.12.12 > 10.12.12.11: GREv0, proto IPv4 (0x0800), length 88: 10.0.0.2 > 10.0.0.1: ICMP echo reply, id 8704, seq 0, length 64
        0x0000:  0242 0a0c 0c0b 0242 0a0c 0c0c 0800 4500  .B.....B......E.
        0x0010:  006c 9e3b 4000 ff2f b0f8 0a0c 0c0c 0a0c  .l.;@../........
        0x0020:  0c0b 0000 0800 4500 0054 3642 0000 4001  ......E..T6B..@.
        0x0030:  3065 0a00 0002 0a00 0001 0000 2513 2200  0e..........%.".
        0x0040:  0000 3e5e 7a8e 0000 0000 0000 0000 0000  ..>^z...........
        0x0050:  0000 0000 0000 0000 0000 0000 0000 0000  ................
        0x0060:  0000 0000 0000 0000 0000 0000 0000 0000  ................
        0x0070:  0000 0000 0000 0000 0000                 ..........
^C      
2 packets captured
2 packets received by filter
0 packets dropped by kernel
```

Now when installing xdp_gre.o in container r2, its tcpdump no longer shows incoming GRE packets, 
but just the ICMP echo requests.

Compiling and loading the XDP gre decap application on r2:

```
clang -Wall -O2 -c -g -target bpf -c xdp-gre.c -o xdp-gre.o                                    
llvm-objdump -S xdp-gre.o                                                                      
docker cp xdp-gre.o r2:/                                                                       
docker exec r2 ip link set dev eth0 xdp off                                                    
docker exec r2 ip link set dev eth0 xdp obj /xdp-gre.o sec gre_decap                           
docker exec r2 ip link show dev eth0                                                           
```

While ping is running on r1, tcpdump changes after the XDP decap object is loaded and activated:

```
$ docker exec -ti r2 tcpdump -n -i eth0
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode                             
listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes                              
12:47:39.372149 IP 10.12.12.11 > 10.12.12.12: GREv0, length 88: IP 10.0.0.1 > 10.0.0.2: ICMP echo request, id 14080, seq 49, length 64
12:47:39.372178 IP 10.12.12.12 > 10.12.12.11: GREv0, length 88: IP 10.0.0.2 > 10.0.0.1: ICMP echo reply, id 14080, seq 49, length 64
12:47:40.372283 IP 10.12.12.11 > 10.12.12.12: GREv0, length 88: IP 10.0.0.1 > 10.0.0.2: ICMP echo request, id 14080, seq 50, length 64
12:47:40.372312 IP 10.12.12.12 > 10.12.12.11: GREv0, length 88: IP 10.0.0.2 > 10.0.0.1: ICMP echo reply, id 14080, seq 50, length 64
12:47:41.372512 IP 10.12.12.11 > 10.12.12.12: GREv0, length 88: IP 10.0.0.1 > 10.0.0.2: ICMP echo request, id 14080, seq 51, length 64
12:47:41.372554 IP 10.12.12.12 > 10.12.12.11: GREv0, length 88: IP 10.0.0.2 > 10.0.0.1: ICMP echo reply, id 14080, seq 51, length 64
12:47:42.372695 IP 10.12.12.11 > 10.12.12.12: GREv0, length 88: IP 10.0.0.1 > 10.0.0.2: ICMP echo request, id 14080, seq 52, length 64
12:47:42.372725 IP 10.12.12.12 > 10.12.12.11: GREv0, length 88: IP 10.0.0.2 > 10.0.0.1: ICMP echo reply, id 14080, seq 52, length 64
12:47:43.372843 IP 10.12.12.11 > 10.12.12.12: GREv0, length 88: IP 10.0.0.1 > 10.0.0.2: ICMP echo request, id 14080, seq 53, length 64
12:47:43.372882 IP 10.12.12.12 > 10.12.12.11: GREv0, length 88: IP 10.0.0.2 > 10.0.0.1: ICMP echo reply, id 14080, seq 53, length 64
12:47:43.529529 IP6 fe80::42:aff:fe0c:c0c > ff02::16: HBH ICMP6, multicast listener report v2, 2 group record(s), length 48
12:47:43.761518 IP6 fe80::42:aff:fe0c:c0c > ff02::16: HBH ICMP6, multicast listener report v2, 2 group record(s), length 48
12:47:44.289561 IP6 fe80::42:aff:fe0c:c0c > ff02::16: HBH ICMP6, multicast listener report v2, 2 group record(s), length 48

# now the ech requests "show" without the GRE header, which has been stripped by the XDP app

12:47:44.373048 IP 10.0.0.1 > 10.0.0.2: ICMP echo request, id 14080, seq 54, length 64                 
12:47:44.373085 IP 10.12.12.12 > 10.12.12.11: GREv0, length 88: IP 10.0.0.2 > 10.0.0.1: ICMP echo reply, id 14080, seq 54, length 64
12:47:45.373247 IP 10.0.0.1 > 10.0.0.2: ICMP echo request, id 14080, seq 55, length 64                 
12:47:45.373270 IP 10.12.12.12 > 10.12.12.11: GREv0, length 88: IP 10.0.0.2 > 10.0.0.1: ICMP echo reply, id 14080, seq 55, length 64
12:47:46.373421 IP 10.0.0.1 > 10.0.0.2: ICMP echo request, id 14080, seq 56, length 64                 
12:47:46.373445 IP 10.12.12.12 > 10.12.12.11: GREv0, length 88: IP 10.0.0.2 > 10.0.0.1: ICMP echo reply, id 14080, seq 56, length 64
12:47:47.373652 IP 10.0.0.1 > 10.0.0.2: ICMP echo request, id 14080, seq 57, length 64                 
12:47:47.373686 IP 10.12.12.12 > 10.12.12.11: GREv0, length 88: IP 10.0.0.2 > 10.0.0.1: ICMP echo reply, id 14080, seq 57, length 64
^C                                                                                                     
```

Log of the installation below.

```
clang -Wall -O2 -c -g -target bpf -c xdp-gre.c -o xdp-gre.o
llvm-objdump -S xdp-gre.o

xdp-gre.o:	file format ELF64-BPF


Disassembly of section gre_decap:

0000000000000000 gre_decap_func:
; int gre_decap_func(struct xdp_md *ctx) {
       0:	bf 17 00 00 00 00 00 00	r7 = r1
       1:	b7 06 00 00 02 00 00 00	r6 = 2
;   void *data_end = (void *)(long)ctx->data_end;
       2:	61 72 04 00 00 00 00 00	r2 = *(u32 *)(r7 + 4)
;   void *data = (void *)(long)ctx->data;
       3:	61 71 00 00 00 00 00 00	r1 = *(u32 *)(r7 + 0)
;   if (data + ipsize > data_end) {
       4:	bf 13 00 00 00 00 00 00	r3 = r1
       5:	07 03 00 00 22 00 00 00	r3 += 34
       6:	2d 23 2b 00 00 00 00 00	if r3 > r2 goto +43 <LBB0_6>
;   if (ip->protocol != IPPROTO_GRE) {
       7:	71 13 17 00 00 00 00 00	r3 = *(u8 *)(r1 + 23)
       8:	55 03 29 00 2f 00 00 00	if r3 != 47 goto +41 <LBB0_6>
       9:	18 06 00 00 ff ff ff ff 00 00 00 00 00 00 00 00	r6 = 4294967295 ll
;   if (neweth + 12 > data_end) {
      11:	bf 13 00 00 00 00 00 00	r3 = r1
      12:	07 03 00 00 24 00 00 00	r3 += 36
      13:	2d 23 24 00 00 00 00 00	if r3 > r2 goto +36 <LBB0_6>
;   __builtin_memcpy(neweth, eth, 12);
      14:	71 12 0b 00 00 00 00 00	r2 = *(u8 *)(r1 + 11)
      15:	73 21 23 00 00 00 00 00	*(u8 *)(r1 + 35) = r2
      16:	71 12 0a 00 00 00 00 00	r2 = *(u8 *)(r1 + 10)
      17:	73 21 22 00 00 00 00 00	*(u8 *)(r1 + 34) = r2
      18:	71 12 09 00 00 00 00 00	r2 = *(u8 *)(r1 + 9)
      19:	73 21 21 00 00 00 00 00	*(u8 *)(r1 + 33) = r2
      20:	71 12 08 00 00 00 00 00	r2 = *(u8 *)(r1 + 8)
      21:	73 21 20 00 00 00 00 00	*(u8 *)(r1 + 32) = r2
      22:	71 12 07 00 00 00 00 00	r2 = *(u8 *)(r1 + 7)
      23:	73 21 1f 00 00 00 00 00	*(u8 *)(r1 + 31) = r2
      24:	71 12 06 00 00 00 00 00	r2 = *(u8 *)(r1 + 6)
      25:	73 21 1e 00 00 00 00 00	*(u8 *)(r1 + 30) = r2
      26:	71 12 05 00 00 00 00 00	r2 = *(u8 *)(r1 + 5)
      27:	73 21 1d 00 00 00 00 00	*(u8 *)(r1 + 29) = r2
      28:	71 12 04 00 00 00 00 00	r2 = *(u8 *)(r1 + 4)
      29:	73 21 1c 00 00 00 00 00	*(u8 *)(r1 + 28) = r2
      30:	71 12 03 00 00 00 00 00	r2 = *(u8 *)(r1 + 3)
      31:	73 21 1b 00 00 00 00 00	*(u8 *)(r1 + 27) = r2
      32:	71 12 02 00 00 00 00 00	r2 = *(u8 *)(r1 + 2)
      33:	73 21 1a 00 00 00 00 00	*(u8 *)(r1 + 26) = r2
      34:	71 12 01 00 00 00 00 00	r2 = *(u8 *)(r1 + 1)
      35:	73 21 19 00 00 00 00 00	*(u8 *)(r1 + 25) = r2
      36:	71 12 00 00 00 00 00 00	r2 = *(u8 *)(r1 + 0)
      37:	73 21 18 00 00 00 00 00	*(u8 *)(r1 + 24) = r2
;   if (bpf_xdp_adjust_head(ctx, 24))
      38:	bf 71 00 00 00 00 00 00	r1 = r7
      39:	b7 02 00 00 18 00 00 00	r2 = 24
      40:	85 00 00 00 2c 00 00 00	call 44
      41:	67 00 00 00 20 00 00 00	r0 <<= 32
      42:	77 00 00 00 20 00 00 00	r0 >>= 32
      43:	55 00 06 00 00 00 00 00	if r0 != 0 goto +6 <LBB0_6>
;   data_end = (void *)(long)ctx->data_end;
      44:	61 71 04 00 00 00 00 00	r1 = *(u32 *)(r7 + 4)
;   data = (void *)(long)ctx->data;
      45:	61 72 00 00 00 00 00 00	r2 = *(u32 *)(r7 + 0)
;   if (data + 1 > data_end)
      46:	07 02 00 00 01 00 00 00	r2 += 1
      47:	b7 06 00 00 ff ff ff ff	r6 = -1
      48:	2d 12 01 00 00 00 00 00	if r2 > r1 goto +1 <LBB0_6>
      49:	b7 06 00 00 02 00 00 00	r6 = 2

0000000000000190 LBB0_6:
; }
      50:	bf 60 00 00 00 00 00 00	r0 = r6
      51:	95 00 00 00 00 00 00 00	exit
docker cp xdp-gre.o r2:/
docker exec r2 ip link set dev eth0 xdp off
docker exec r2 ip link set dev eth0 xdp obj /xdp-gre.o sec gre_decap

BTF debug data section '.BTF' rejected: Invalid argument (22)!
 - Length:       891
Verifier analysis:

magic: 0xeb9f
version: 1
flags: 0x0
hdr_len: 24
type_off: 0
type_len: 256
str_off: 256
str_len: 611
btf_total_size: 891
[1] PTR (anon) type_id=2
[2] STRUCT xdp_md size=20 vlen=5
	data type_id=3 bits_offset=0
	data_end type_id=3 bits_offset=32
	data_meta type_id=3 bits_offset=64
	ingress_ifindex type_id=3 bits_offset=96
	rx_queue_index type_id=3 bits_offset=128
[3] TYPEDEF __u32 type_id=4
[4] INT unsigned int size=4 bits_offset=0 nr_bits=32 encoding=(none)
[5] FUNC_PROTO (anon) return=6 args=(1 ctx)
[6] INT int size=4 bits_offset=0 nr_bits=32 encoding=SIGNED
[7] FUNC gre_decap_func type_id=5 vlen != 0

docker exec r2 ip link show dev eth0
117: eth0@if118: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 xdp qdisc noqueue state UP mode DEFAULT group default 
    link/ether 02:42:0a:0c:0c:0c brd ff:ff:ff:ff:ff:ff link-netnsid 0
    prog/xdp id 40 tag 207371200af7bf08 jited 




