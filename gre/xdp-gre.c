/* SPDX-License-Identifier: GPL-2.0 */

#include <linux/bpf.h>
#include <linux/in.h>
#include <linux/if_ether.h>
#include <linux/ip.h>

#define SEC(NAME) __attribute__((section(NAME), used))

// from /usr/include/bpf/bpf_helper_defs.h:
static int (*bpf_xdp_adjust_head)(struct xdp_md *xdp_md, int delta) = (void *) 44;
static int (*bpf_fib_lookup)(void *ctx, struct bpf_fib_lookup *params, int plen, __u32 flags) = (void *) 69;
static int (*bpf_redirect)(__u32 ifindex, __u64 flags) = (void *) 23;
static int (*bpf_redirect_map)(void *map, __u32 key, __u64 flags) = (void *) 51;
static __s64 (*bpf_csum_diff)(__be32 *from, __u32 from_size, __be32 *to, __u32 to_size, __wsum seed) = (void *) 28;

// from /usr/include/bpf/bpf_endian.h:
#define __bpf_ntohs(x) __builtin_bswap16(x)
#define __bpf_htons(x) __builtin_bswap16(x)

#define AF_INET 2

/* from include/net/ip.h */
static __always_inline int ip_decrease_ttl(struct iphdr *iph)
{
  __u32 check = iph->check;
  check += __bpf_htons(0x0100);
  iph->check = (__u16)(check + (check >= 0xFFFF));
  return --iph->ttl;
}

static __always_inline __u16 csum_fold_helper(__u32 csum)
{
    return ~((csum & 0xffff) + (csum >> 16));
}

static __always_inline void ipv4_csum(void *data_start, int data_size,
                  __u32 *csum)
{
    *csum = bpf_csum_diff(0, 0, data_start, data_size, *csum);
    *csum = csum_fold_helper(*csum);
}

 struct bpf_map_def {
   unsigned int type;
   unsigned int key_size;
   unsigned int value_size;
   unsigned int max_entries;
   unsigned int map_flags;
 };                                                                                                                

struct bpf_map_def SEC("maps") tx_port = {                                                                        
  .type = BPF_MAP_TYPE_DEVMAP,
  .key_size = sizeof(int),
  .value_size = sizeof(int),
  .max_entries = 256,
};

SEC("gre_router")

int gre_router_func(struct xdp_md *ctx) {

  int ipsize = 0;
  int action = XDP_PASS;

  void *data = (void *)(long)ctx->data;
  void *data_end = (void *)(long)ctx->data_end;

  struct ethhdr *eth = data;
  struct iphdr *ip = data + sizeof(*eth);
  struct iphdr *iip = data + sizeof(*eth) + 24;
  struct bpf_fib_lookup fib_params = {};
  __u32 csum = 0;
  int rc;

  ipsize = sizeof(*eth);
  ipsize += sizeof(struct iphdr);
  if (data + ipsize > data_end) {
    // not an ip packet, too short. Pass it on
    return XDP_PASS;
  }

  // let anything other than GRE go thru nharmed
  if (ip->protocol != IPPROTO_GRE) {
    return XDP_PASS;
  }

  // do an IP lookup on the inner packets destIP
  // need to enhance this for IPv6 too
  if ((void *)iip + sizeof(struct iphdr) > data_end) {
    return XDP_PASS;
  }
  fib_params.family = AF_INET;
  fib_params.tos    = iip->tos;
  fib_params.l4_protocol = iip->protocol;
  fib_params.sport = 0;
  fib_params.dport = 0;
  fib_params.tot_len = __bpf_ntohs(iip->tot_len);
  fib_params.ipv4_src = iip->saddr;
  fib_params.ipv4_dst = iip->daddr;
  fib_params.ifindex = ctx->ingress_ifindex;
  rc = bpf_fib_lookup(ctx, &fib_params, sizeof(fib_params), 0);
  void *neweth = (void *)(data + 24);
  switch (rc) {
    case BPF_FIB_LKUP_RET_SUCCESS:
      // need to send the GRE packet to the resolved nh
      ip_decrease_ttl(ip);
      __builtin_memcpy(eth->h_dest, fib_params.dmac, ETH_ALEN);
      __builtin_memcpy(eth->h_source, fib_params.smac, ETH_ALEN);
      // adjust ipv4 header checksum
//      ip->saddr = fib_params.ipv4_src;
//      ip->daddr = fib_params.ipv4_dst;
//      csum = 0;
//        ip->check = 0;
//      ipv4_csum(ip, sizeof(struct iphdr), &csum);
//      ip->check = csum;
      action = bpf_redirect_map(&tx_port, fib_params.ifindex, 0);
//      action = XDP_DROP;
      break;
    case BPF_FIB_LKUP_RET_BLACKHOLE:
    case BPF_FIB_LKUP_RET_UNREACHABLE:
    case BPF_FIB_LKUP_RET_PROHIBIT:
      action = XDP_DROP;
      break;
    case BPF_FIB_LKUP_RET_NOT_FWDED:
    case BPF_FIB_LKUP_RET_FWD_DISABLED:
    case BPF_FIB_LKUP_RET_UNSUPP_LWT:
    case BPF_FIB_LKUP_RET_NO_NEIGH:
    case BPF_FIB_LKUP_RET_FRAG_NEEDED:
      // packet destined for this host
      // remove the GRE header by moving the Eth hdr up
      if (neweth + 12 > data_end) {
        return -1;
      }

      __builtin_memcpy(neweth, eth, 12);
      // move the packet start pointer
      if (bpf_xdp_adjust_head(ctx, 24))
        return -1;

      data = (void *)(long)ctx->data;
      data_end = (void *)(long)ctx->data_end;
      if (data + 1 > data_end)
        return -1;
      break;
  }

  return action;
}

SEC("gre_decap")

int gre_decap_func(struct xdp_md *ctx) {
  int ipsize = 0;

  void *data = (void *)(long)ctx->data;
  void *data_end = (void *)(long)ctx->data_end;

  struct ethhdr *eth = data;
  struct iphdr *ip = data + sizeof(*eth);

  ipsize = sizeof(*eth);
  ipsize += sizeof(struct iphdr);
  if (data + ipsize > data_end) {
    // not an ip packet, too short. Pass it on
    return XDP_PASS;
  }

  // let anything other than GRE go thru nharmed
  if (ip->protocol != IPPROTO_GRE) {
    return XDP_PASS;
  }

  // remove the GRE header by moving the Eth hdr up
  void *neweth = (void *)(data + 24);
  if (neweth + 12 > data_end) {
    return -1;
  }

  __builtin_memcpy(neweth, eth, 12);
  // move the packet start pointer
  if (bpf_xdp_adjust_head(ctx, 24))
    return -1;

  data = (void *)(long)ctx->data;
  data_end = (void *)(long)ctx->data_end;
  if (data + 1 > data_end)
    return -1;

  // need to do a route lookup if this shall work beyond a simple terminating 
  // device
  
  return XDP_PASS;
}

SEC("gre_pass")

int gre_pass_func(struct xdp_md *ctx) {
  
  return XDP_PASS;
}

char __license[] SEC("license") = "GPL";
