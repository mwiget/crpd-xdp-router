#!/bin/bash     

INTERFACE="${INTERFACE:-eth0}"
myip=$(hostname -I|cut -d' ' -f1)
echo "myip=$myip"

siteid=$(hostname|cut -d- -f4)
case "$siteid" in
  ''|*[!0-9]*) siteid=$(echo $myip|cut -d. -f4); echo "siteid=$siteid (from IP addr)" ;;
  *) echo siteid=$siteid ;;
esac

echo "check first if $GRE is local (has arp entry) ..."
ping -c 3 $GRE
macgw=$(arp -na|grep $GRE|cut -d' ' -f4)
if [ -z "$macgw" ]; then
  echo "no. Using default gateway instead ..."
  ipgw=$(ip r|grep default|cut -d' ' -f3)
  ping -c 3 $ipgw   # need an ARP entry
  macgw=$(arp -na|grep $ipgw|cut -d' ' -f4)
fi
echo "use $macgw to reach $ipgw"

srcmac=$(cat /sys/class/net/$INTERFACE/address)
echo "my mac address is $srcmac"

echo "GRE tunnel endpoint is set via env var GRE to $GRE"

cat > /root/site.h <<EOF
#define src_mac   $srcmac
#define dst_mac   $macgw

#define src_ip    $myip
#define dst_ip    $GRE

#define site_id $siteid
EOF

if [ -z "$R1" ] || [ -z "$R2" ]; then
  echo "please set R1 and R2 to remote BGP peers"
  grep -v neighbor /root/juniper.conf > /config/juniper.conf
else
  echo "BGP sessions with R1=$R1 and R2=$R2"
  envsubst < /root/juniper.conf > /config/juniper.conf
fi

echo "generating host routes for 64k trafgen IP's ..."
cat >> /config/juniper.conf <<EOF
routing-options {
static {
EOF

for i in {0..255}; do
  for j in {0..255}; do
    echo "route 10.$siteid.$i.$j/32 discard;" >> /config/juniper.conf
  done
done
echo "}}" >> /config/juniper.conf

echo "static route generation completed in $SECONDS"

service ssh restart

if [ -z "$fullspeed" ]; then
  echo "starting low speed trafgen (1pps) ..."
  python3 ./send_gre_in_udp.py --int $INTERFACE --gre $GRE --site $siteid &
#  trafgen -o $INTERFACE -p --no-sock-mem --conf gre.cfg -t 1s &
else
  echo "starting full speed trafgen within 60 seconds ..."
  echo "blocking incoming GRE over UDP traffic"
  iptables -I INPUT -p udp  --dport 4754 -j DROP
  (sleep 60; trafgen -o $INTERFACE -p --no-sock-mem --conf gre.cfg) &
fi


export > /etc/envvars
exec /sbin/runit-init 0
