# crpd-trafgen

cRPD container with trafgen and static routes.

```
$ trafgen --out gre.pcap -n 3 -p --no-sock-mem --conf gre.cfg
1 packets to schedule
96 bytes in total
Running! Hang up with ^C!


3 packets outgoing
288 bytes outgoing
0 sec, 41763 usec on CPU0 (3 packets)
```

```
$ tcpdump -n -r gre.pcap -evXX
21:22:32.288060 02:42:0a:0a:0a:02 > 02:42:d9:a1:75:94, ethertype IPv4 (0x0800), length 96: (tos 0x0, id 0, offset 0, flags [none], proto GRE (47), length 82)
10.10.10.2 > 10.10.10.11: GREv0, Flags [none], proto IPv4 (0x0800), length 62
(tos 0x0, ttl 64, id 52143, offset 0, flags [DF], proto UDP (17), length 58)
192.168.1.100.7 > 10.2.117.125.7: UDP, length 10
0x0000:  0242 d9a1 7594 0242 0a0a 0a02 0800 4500  .B..u..B......E.
0x0010:  0052 0000 0000 002f 925d 0a0a 0a02 0a0a  .R...../.]......
0x0020:  0a0b 0000 0800 4500 003a cbaf 4000 4011  ......E..:..@.@.
0x0030:  2d78 c0a8 0164 0a02 757d 0007 0007 0012  -x...d..u}......
0x0040:  0063 7270 642d 7864 702d 726f 7574 6572  .crpd-xdp-router
0x0050:  2074 7261 6667 656e 2062 6c61 7374 6572  .trafgen.blaster
21:22:32.288062 02:42:0a:0a:0a:02 > 02:42:d9:a1:75:94, ethertype IPv4 (0x0800), length 96: (tos 0x0, id 0, offset 0, flags [none], proto GRE (47), length 82)
10.10.10.2 > 10.10.10.11: GREv0, Flags [none], proto IPv4 (0x0800), length 62
(tos 0x0, ttl 64, id 63354, offset 0, flags [DF], proto UDP (17), length 58)
192.168.1.100.7 > 10.2.90.24.7: UDP, length 10
0x0000:  0242 d9a1 7594 0242 0a0a 0a02 0800 4500  .B..u..B......E.
0x0010:  0052 0000 0000 002f 925d 0a0a 0a02 0a0a  .R...../.]......
0x0020:  0a0b 0000 0800 4500 003a f77a 4000 4011  ......E..:.z@.@.
0x0030:  1d12 c0a8 0164 0a02 5a18 0007 0007 0012  .....d..Z.......
0x0040:  0063 7270 642d 7864 702d 726f 7574 6572  .crpd-xdp-router
0x0050:  2074 7261 6667 656e 2062 6c61 7374 6572  .trafgen.blaster
21:22:32.288063 02:42:0a:0a:0a:02 > 02:42:d9:a1:75:94, ethertype IPv4 (0x0800), length 96: (tos 0x0, id 0, offset 0, flags [none], proto GRE (47), length 82)
10.10.10.2 > 10.10.10.11: GREv0, Flags [none], proto IPv4 (0x0800), length 62
(tos 0x0, ttl 64, id 8655, offset 0, flags [DF], proto UDP (17), length 58)
192.168.1.100.7 > 10.2.21.74.7: UDP, length 10
0x0000:  0242 d9a1 7594 0242 0a0a 0a02 0800 4500  .B..u..B......E.
0x0010:  0052 0000 0000 002f 925d 0a0a 0a02 0a0a  .R...../.]......
0x0020:  0a0b 0000 0800 4500 003a 21cf 4000 4011  ......E..:!.@.@.
0x0030:  378c c0a8 0164 0a02 154a 0007 0007 0012  7....d...J......
0x0040:  0063 7270 642d 7864 702d 726f 7574 6572  .crpd-xdp-router
0x0050:  2074 7261 6667 656e 2062 6c61 7374 6572  .trafgen.blaster
```

## References

Blog post on adding RFC 8086 support in Linux to decode packets via tcpdump:
https://www.bortzmeyer.org/8086.html

To receive and decode GRE over UDP packets, use:

```
sudo modprobe fou
sudo ip fou add port 4754 ipproto 47
sudo ip link add name tun1 type gre remote 192.168.0.50 local 192.168.0.49 ttl 255 encap fou encap-sport auto encap-dport 4754
sudo ip link set tun1 up
```

Decode incoming packets:

```
$ sudo tcpdump -n -i tun1 -veX
tcpdump: listening on tun1, link-type LINUX_SLL (Linux cooked), capture size 262144 bytes
04:01:58.983365  In ethertype IPv4 (0x0800), length 75: (tos 0x0, ttl 64, id 23442, offset 0, flags [DF], proto UDP (17), length 58)
192.168.1.100.7 > 10.2.18.218.7: UDP, length 12
0x0000:  4500 003a 5b92 4000 4011 0039 c0a8 0164  E..:[.@.@..9...d
0x0010:  0a02 12da 0007 0007 0014 0000 6372 7064  ............crpd
0x0020:  2d78 6470 2d72 6f75 7465 7220 7472 6166  -xdp-router.traf
0x0030:  6765 6e20 626c 6173 7465 72              gen.blaster
04:01:58.983392  In ethertype IPv4 (0x0800), length 75: (tos 0x0, ttl 64, id 54236, offset 0, flags [DF], proto UDP (17), length 58)
192.168.1.100.7 > 10.2.141.235.7: UDP, length 12
0x0000:  4500 003a d3dc 4000 4011 0cdd c0a8 0164  E..:..@.@......d
0x0010:  0a02 8deb 0007 0007 0014 0000 6372 7064  ............crpd
0x0020:  2d78 6470 2d72 6f75 7465 7220 7472 6166  -xdp-router.traf
0x0030:  6765 6e20 626c 6173 7465 72              gen.blaster
04:01:58.983398  In ethertype IPv4 (0x0800), length 75: (tos 0x0, ttl 64, id 63906, offset 0, flags [DF], proto UDP (17), length 58)
192.168.1.100.7 > 10.2.190.204.7: UDP, length 12
0x0000:  4500 003a f9a2 4000 4011 b635 c0a8 0164  E..:..@.@..5...d
0x0010:  0a02 becc 0007 0007 0014 0000 6372 7064  ............crpd
0x0020:  2d78 6470 2d72 6f75 7465 7220 7472 6166  -xdp-router.traf
0x0030:  6765 6e20 626c 6173 7465 72              gen.blaster
^C
3 packets captured
3 packets received by filter
0 packets dropped by kernel
```

GRE over UDP isn't properly decoded by tcpdump, so the following hack makes the content visible:

A script captures on eth0, chops the outer IP and UDP header via editcap, then pipes it into tcpdump.
Due to buffering using pipes, the script stops after 2 packets and starts all over...

```
#!/bin/bash
set -e
while true; do
tcpdump -q -n -i eth0 -c 2 -s 1500 -l -w- udp port 4754 2>/dev/null |editcap -C 14:32  - - |tcpdump -n -l -q -r - 2>/dev/null
done
```

Initially only random icmp id's can be seen, but suddenly the same id is seen twice. These come from the routed packet
back from the reflector. The outer header shown in normal pcap will show packets are coming back (see below):

```
$ docker exec -ti dc_c_1 /root/tcpdump.sh                                                               
19:15:48.587980 IP 10.10.10.3 > 10.3.54.84: ICMP echo request, id 8097, seq 0, length 31                                                
19:15:49.698587 IP 10.10.10.3 > 10.3.53.99: ICMP echo request, id 19995, seq 0, length 31                                               
19:15:50.826572 IP 10.10.10.3 > 10.3.243.147: ICMP echo request, id 51702, seq 0, length 31                                             
19:15:51.966769 IP 10.10.10.3 > 10.3.201.192: ICMP echo request, id 51440, seq 0, length 31                                             
19:15:54.207681 IP 10.10.10.3 > 10.3.12.23: ICMP echo request, id 48404, seq 0, length 31                                               
19:15:55.278665 IP 10.10.10.3 > 10.3.98.177: ICMP echo request, id 6199, seq 0, length 31                                               
19:15:56.293594 IP 10.10.10.3 > 10.3.79.210: ICMP echo request, id 6301, seq 0, length 31                                               
19:15:57.463906 IP 10.10.10.3 > 10.3.167.132: ICMP echo request, id 63038, seq 0, length 31                                             
19:15:58.590956 IP 10.10.10.3 > 10.3.98.0: ICMP echo request, id 13301, seq 0, length 31                                                
19:15:59.715477 IP 10.10.10.3 > 10.3.115.138: ICMP echo request, id 9093, seq 0, length 31                                              
19:16:00.834992 IP 10.10.10.3 > 10.3.250.66: ICMP echo request, id 58239, seq 0, length 31                                              
19:16:01.939023 IP 10.10.10.3 > 10.3.20.8: ICMP echo request, id 16358, seq 0, length 31                                                
19:16:03.083039 IP 10.10.10.3 > 10.3.31.24: ICMP echo request, id 29024, seq 0, length 31                                               
19:16:04.203147 IP 10.10.10.3 > 10.3.140.59: ICMP echo request, id 28511, seq 0, length 31                                              
19:16:05.319071 IP 10.10.10.3 > 10.3.158.194: ICMP echo request, id 56755, seq 0, length 31                                             
19:16:06.458914 IP 10.10.10.3 > 10.3.37.163: ICMP echo request, id 6883, seq 0, length 31                                               
19:16:07.598945 IP 10.10.10.3 > 10.3.190.245: ICMP echo request, id 54117, seq 0, length 31                                             
19:16:08.667930 IP 10.10.10.3 > 10.3.180.0: ICMP echo request, id 1382, seq 0, length 31                                                
19:16:09.711686 IP 10.10.10.3 > 10.3.184.4: ICMP echo request, id 58072, seq 0, length 31                                               
19:16:10.803195 IP 10.10.10.3 > 10.3.222.118: ICMP echo request, id 17516, seq 0, length 31                                             
19:16:11.911037 IP 10.10.10.3 > 10.3.32.213: ICMP echo request, id 19206, seq 0, length 31                                              
19:16:11.911116 IP 10.10.10.3 > 10.3.32.213: ICMP echo request, id 19206, seq 0, length 31                                              
19:16:13.038944 IP 10.10.10.3 > 10.3.187.72: ICMP echo request, id 37794, seq 0, length 31                                              
19:16:13.039020 IP 10.10.10.3 > 10.3.187.72: ICMP echo request, id 37794, seq 0, length 31                                              
19:16:14.146879 IP 10.10.10.3 > 10.3.20.62: ICMP echo request, id 24046, seq 0, length 31                                               
19:16:14.146961 IP 10.10.10.3 > 10.3.20.62: ICMP echo request, id 24046, seq 0, length 31                                               
19:16:15.262987 IP 10.10.10.3 > 10.3.160.155: ICMP echo request, id 25820, seq 0, length 31                                             
19:16:15.263067 IP 10.10.10.3 > 10.3.160.155: ICMP echo request, id 25820, seq 0, length 31                                             
19:16:16.383035 IP 10.10.10.3 > 10.3.117.228: ICMP echo request, id 50134, seq 0, length 31                                             
19:16:16.383121 IP 10.10.10.3 > 10.3.117.228: ICMP echo request, id 50134, seq 0, length 31                                             
```

and just the outer headers:

```
$ docker exec -ti dc_c_1 tcpdump -n -i eth0 -evXX
tcpdump: listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes
19:20:05.995109 02:42:0a:0a:0a:03 > 02:42:0a:0a:0a:0b, ethertype IPv4 (0x0800), length 97: (tos 0x0, ttl 64, id 1, offset 0, flags [none], proto UDP (17), length 83)
    10.10.10.3.9144 > 10.10.10.11.4754: UDP, length 55
        0x0000:  0242 0a0a 0a0b 0242 0a0a 0a03 0800 4500  .B.....B......E.
        0x0010:  0053 0001 0000 4011 5278 0a0a 0a03 0a0a  .S....@.Rx......
        0x0020:  0a0b 23b8 1292 003f 9904 0000 0800 4500  ..#....?......E.
        0x0030:  0033 0001 0000 4001 1233 0a0a 0a03 0a03  .3....@..3......
        0x0040:  4a87 0800 3d85 fe36 0000 6865 6c6c 6f20  J...=..6..hello.
        0x0050:  6672 6f6d 2063 7270 645f 7472 6166 6765  from.crpd_trafge
        0x0060:  6e                                       n
19:20:05.995196 02:42:0a:0a:0a:0b > 02:42:0a:0a:0a:03, ethertype IPv4 (0x0800), length 97: (tos 0x0, ttl 63, id 1, offset 0, flags [none], proto UDP (17), length 83)
    10.10.10.11.9144 > 10.10.10.3.4754: UDP, length 55
        0x0000:  0242 0a0a 0a03 0242 0a0a 0a0b 0800 4500  .B.....B......E.
        0x0010:  0053 0001 0000 3f11 5378 0a0a 0a0b 0a0a  .S....?.Sx......
        0x0020:  0a03 23b8 1292 003f 9904 0000 0800 4500  ..#....?......E.
        0x0030:  0033 0001 0000 4001 1233 0a0a 0a03 0a03  .3....@..3......
        0x0040:  4a87 0800 3d85 fe36 0000 6865 6c6c 6f20  J...=..6..hello.
        0x0050:  6672 6f6d 2063 7270 645f 7472 6166 6765  from.crpd_trafge
        0x0060:  6e                                       n
```
