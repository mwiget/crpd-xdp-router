#!/usr/bin/python3

import argparse
from scapy.all import *

parser = argparse.ArgumentParser()
parser.add_argument('--int', help='interface',
                    dest='interface', default='eth0')
parser.add_argument('--gre', help='remote gre tunnel ip',
                    dest='gre_ip', default='127.0.0.1')
parser.add_argument('--site', help='siteid (0..255) used in 10.siteid.x.y',
                    type=int, dest='site_id', default=0)
args = parser.parse_args()

payload = "hello from crpd_trafgen"

while True:
    dst_ip = '10.%d.%d.%d' % (args.site_id, RandByte(), RandByte())
    packet = \
        IP(dst=args.gre_ip) / \
        UDP(sport=RandShort(), dport=4754) / \
        GRE(key_present=0, seqnum_present=0) / \
        IP(dst=dst_ip)/ICMP(type=8, code=0, id=RandShort())/payload
    sr(packet, iface=args.interface, timeout=1)
