/* SPDX-License-Identifier: GPL-2.0 */

#include <linux/bpf.h>
#include <linux/in.h>
#include <linux/if_ether.h>
#include <linux/ip.h>
#include <linux/udp.h>
#include <bpf/bpf_helpers.h>
#include <bpf/bpf_endian.h>

#include "xdp_stats_kern_user.h"
#include "parsing_helpers.h"
#include "rewrite_helpers.h"
#include "xdp_stats_kern.h"

#define AF_INET 2

/* from include/net/ip.h */
static __always_inline int ip_decrease_ttl(struct iphdr *iph)
{
  __u32 check = iph->check;
  check += __bpf_htons(0x0100);
  iph->check = (__u16)(check + (check >= 0xFFFF));
  return --iph->ttl;
}


static __always_inline __u16 csum_fold_helper(__u32 csum)
{
  __u32 sum;
  sum = (csum>>16) + (csum & 0xffff);
  sum += (sum>>16);
  return ~sum;
}

static __always_inline void ipv4_csum(void *data_start, int data_size,
    __u32 *csum)
{
  *csum = bpf_csum_diff(0, 0, data_start, data_size, *csum);
  *csum = csum_fold_helper(*csum);
}

/*
 * The icmp_checksum_diff function takes pointers to old and new structures and
 * the old checksum and returns the new checksum.  It uses the bpf_csum_diff
 * helper to compute the checksum difference. Note that the sizes passed to the
 * bpf_csum_diff helper should be multiples of 4, as it operates on 32-bit
 * words.
 */
static __always_inline __u16 icmp_checksum_diff(
		__u16 seed,
		struct icmphdr_common *icmphdr_new,
		struct icmphdr_common *icmphdr_old)
{
	__u32 csum, size = sizeof(struct icmphdr_common);

	csum = bpf_csum_diff((__be32 *)icmphdr_old, size, (__be32 *)icmphdr_new, size, seed);
	return csum_fold_helper(csum);
}

struct bpf_map_def SEC("maps") tx_port = {                                                                        
  .type = BPF_MAP_TYPE_DEVMAP,
  .key_size = sizeof(int),
  .value_size = sizeof(int),
  .max_entries = 256,
};

SEC("xdp_gre")  // GRE over UDP according to RFC 8086

int gre_router_func(struct xdp_md *ctx) {

  int ipsize = 0;
  int action = XDP_PASS;

  void *data = (void *)(long)ctx->data;
  void *data_end = (void *)(long)ctx->data_end;

  struct ethhdr *eth = data;
  struct iphdr *iph = data + sizeof(*eth);
  struct udphdr *udp = data + sizeof(*eth) + 20;
  struct iphdr *iip = data + sizeof(*eth) + 32;
  struct bpf_fib_lookup fib_params = {};
  __u32 csum = 0;
  int rc;

  ipsize = sizeof(*eth);
  ipsize += sizeof(struct iphdr);
  ipsize += sizeof(struct udphdr);
  if (data + ipsize > data_end) {
    // not an ip packet, too short. Pass it on
    return xdp_stats_record_action(ctx, XDP_PASS);
  }

  // only interested in RFC 8086 GRE-in-UDP
  if (iph->protocol != IPPROTO_UDP) {
    return xdp_stats_record_action(ctx, XDP_PASS);
  }

  // let anything other than GRE over UDP go thru unharmed
  if (udp->dest != __bpf_htons(4754)) {
    return xdp_stats_record_action(ctx, XDP_PASS);
  }
  // reaching here !!

  // do an IP lookup on the inner packets destIP
  // need to enhance this for IPv6 too
  if ((void *)iip + sizeof(struct iphdr) > data_end) {
    return xdp_stats_record_action(ctx, XDP_PASS);
  }
  fib_params.family = AF_INET;
  fib_params.tos    = iip->tos;
  fib_params.l4_protocol = iip->protocol;
  fib_params.sport = 0;
  fib_params.dport = 0;
  fib_params.tot_len = __bpf_ntohs(iph->tot_len);
//  fib_params.ipv4_src = iip->saddr;
  fib_params.ipv4_dst = iip->daddr;
  fib_params.ifindex = ctx->ingress_ifindex;
  rc = bpf_fib_lookup(ctx, &fib_params, sizeof(fib_params), 0);
  void *neweth = (void *)(data + 24);
  switch (rc) {
    case BPF_FIB_LKUP_RET_SUCCESS:
      // need to send the GRE packet to the resolved nh
      __builtin_memcpy(eth->h_dest, fib_params.dmac, ETH_ALEN);
      __builtin_memcpy(eth->h_source, fib_params.smac, ETH_ALEN);

      iph->ttl--;
      iph->saddr = iph->daddr;
      iph->daddr = fib_params.ipv4_dst;
      iph->check = 0;
      csum = 0;
      ipv4_csum(iph, sizeof(struct iphdr), &csum);
      iph->check = csum;

      //action = bpf_redirect_map(&tx_port, fib_params.ifindex, 0);
      action = XDP_TX;  // send it back out the same interface
      break;
    case BPF_FIB_LKUP_RET_BLACKHOLE:
    case BPF_FIB_LKUP_RET_UNREACHABLE:
    case BPF_FIB_LKUP_RET_PROHIBIT:
      action = XDP_DROP;
      break;
    case BPF_FIB_LKUP_RET_NOT_FWDED:
    case BPF_FIB_LKUP_RET_FWD_DISABLED:
    case BPF_FIB_LKUP_RET_UNSUPP_LWT:
    case BPF_FIB_LKUP_RET_NO_NEIGH:
    case BPF_FIB_LKUP_RET_FRAG_NEEDED:
      // packet destined for this host
      // remove the GRE header by moving the Eth hdr up
      action = XDP_PASS;
      if (neweth + 12 > data_end) {
        return xdp_stats_record_action(ctx, XDP_DROP);
      }

      __builtin_memcpy(neweth, eth, 12);
      // move the packet start pointer
      if (bpf_xdp_adjust_head(ctx, 24))
        return xdp_stats_record_action(ctx, XDP_DROP);

      data = (void *)(long)ctx->data;
      data_end = (void *)(long)ctx->data_end;
      if (data + 1 > data_end)
        return xdp_stats_record_action(ctx, XDP_DROP);
      break;
  }

  return xdp_stats_record_action(ctx, action);
}

/* Solution to packet03/assignment-1 */
SEC("xdp_icmp_echo")
int xdp_icmp_echo_func(struct xdp_md *ctx)
{
	void *data_end = (void *)(long)ctx->data_end;
	void *data = (void *)(long)ctx->data;
	struct hdr_cursor nh;
	struct ethhdr *eth;
	int eth_type;
	int ip_type;
	int icmp_type;
	struct iphdr *iphdr;
	struct ipv6hdr *ipv6hdr;
	__u16 echo_reply, old_csum;
	struct icmphdr_common *icmphdr;
	struct icmphdr_common icmphdr_old;
	__u32 action = XDP_PASS;

	/* These keep track of the next header type and iterator pointer */
	nh.pos = data;

	/* Parse Ethernet and IP/IPv6 headers */
	eth_type = parse_ethhdr(&nh, data_end, &eth);
	if (eth_type == bpf_htons(ETH_P_IP)) {
		ip_type = parse_iphdr(&nh, data_end, &iphdr);
		if (ip_type != IPPROTO_ICMP)
			goto out;
	} else if (eth_type == bpf_htons(ETH_P_IPV6)) {
		ip_type = parse_ip6hdr(&nh, data_end, &ipv6hdr);
		if (ip_type != IPPROTO_ICMPV6)
			goto out;
	} else {
		goto out;
	}

	/*
	 * We are using a special parser here which returns a stucture
	 * containing the "protocol-independent" part of an ICMP or ICMPv6
	 * header.  For purposes of this Assignment we are not interested in
	 * the rest of the structure.
	 */
	icmp_type = parse_icmphdr_common(&nh, data_end, &icmphdr);
	if (eth_type == bpf_htons(ETH_P_IP) && icmp_type == ICMP_ECHO) {
		/* Swap IP source and destination */
		swap_src_dst_ipv4(iphdr);
		echo_reply = ICMP_ECHOREPLY;
	} else if (eth_type == bpf_htons(ETH_P_IPV6)
		   && icmp_type == ICMPV6_ECHO_REQUEST) {
		/* Swap IPv6 source and destination */
		swap_src_dst_ipv6(ipv6hdr);
		echo_reply = ICMPV6_ECHO_REPLY;
	} else {
		goto out;
	}

	/* Swap Ethernet source and destination */
	swap_src_dst_mac(eth);


	/* Patch the packet and update the checksum.*/
	old_csum = icmphdr->cksum;
	icmphdr->cksum = 0;
	icmphdr_old = *icmphdr;
	icmphdr->type = echo_reply;
	icmphdr->cksum = icmp_checksum_diff(~old_csum, icmphdr, &icmphdr_old);

	/* Another, less generic, but a bit more efficient way to update the
	 * checksum is listed below.  As only one 16-bit word changed, the sum
	 * can be patched using this formula: sum' = ~(~sum + ~m0 + m1), where
	 * sum' is a new sum, sum is an old sum, m0 and m1 are the old and new
	 * 16-bit words, correspondingly. In the formula above the + operation
	 * is defined as the following function:
	 *
	 *     static __always_inline __u16 csum16_add(__u16 csum, __u16 addend)
	 *     {
	 *         csum += addend;
	 *         return csum + (csum < addend);
	 *     }
	 *
	 * So an alternative code to update the checksum might look like this:
	 *
	 *     __u16 m0 = * (__u16 *) icmphdr;
	 *     icmphdr->type = echo_reply;
	 *     __u16 m1 = * (__u16 *) icmphdr;
	 *     icmphdr->checksum = ~(csum16_add(csum16_add(~icmphdr->checksum, ~m0), m1));
	 */

	action = XDP_TX;

out:
	return xdp_stats_record_action(ctx, action);
}

SEC("xdp_pass")
int xdp_pass_func(struct xdp_md *ctx)
{
  return xdp_stats_record_action(ctx, XDP_PASS);
}

char __license[] SEC("license") = "GPL";
