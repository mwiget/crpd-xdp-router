#!/bin/ash

set -e  # exit script in case of errors

mount -t bpf bpf /sys/fs/bpf/

ip addr add 10.0.0.1/32 dev lo
ip tunnel add gre1 mode gre remote 10.12.12.12 local 10.12.12.11 ttl 255
ip link set gre1 up
ip addr add 10.0.1.1/24 dev gre1
ip route add 10.0.0.2/32 dev gre1 src 10.0.0.1
ip route add 10.0.0.3/32 dev gre1 src 10.0.0.1
ip addr

tail -f /dev/null
