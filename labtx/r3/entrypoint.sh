#!/bin/ash

set -e  # exit script in case of errors

mount -t bpf bpf /sys/fs/bpf/

ip addr add 10.0.0.3/32 dev lo
ip tunnel add gre3 mode gre remote 10.10.10.12 local 10.10.10.13 ttl 255
ip link set gre3 up
ip addr add 10.0.2.3/24 dev gre3
ip route add 10.0.0.1/32 dev gre3 src 10.0.0.3
ip route add 10.0.0.2/32 dev gre3 src 10.0.0.3
ip addr

tail -f /dev/null
