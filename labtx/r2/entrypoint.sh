#!/bin/ash

set -e  # exit script in case of errors

mount -t bpf bpf /sys/fs/bpf/

ip addr add 10.0.0.2/32 dev lo
#ip tunnel add gre2 mode gre remote 10.10.10.11 local 10.10.10.12 ttl 255
#ip tunnel add gre3 mode gre remote 10.10.10.13 local 10.10.10.12 ttl 255
#ip link set gre2 up
#ip addr add 10.0.1.2/24 dev gre2
#ip link set gre3 up
#ip addr add 10.0.2.2/24 dev gre3
#ip route add 10.0.0.1/32 dev gre2 src 10.0.0.2
#ip route add 10.0.0.3/32 dev gre3 src 10.0.0.2

# do gre "switching" via xdp_router
ip route add 10.0.0.1/32 via 10.10.10.11 src 10.0.0.2
ip route add 10.0.0.3/32 via 10.10.10.13 src 10.0.0.2
ip addr

ulimit -l 1024
xdp_loader -F -d eth0 --progsec xdp_gre

tail -f /dev/null
