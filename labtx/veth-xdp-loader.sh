#!/bin/sh
for veth in $(ls -d /sys/class/net/veth* | cut -d/ -f5)
do
  echo $veth
  sudo ../xdp_loader -d $veth --filename ../xdp_router.o --progsec xdp_pass -F -A
done
