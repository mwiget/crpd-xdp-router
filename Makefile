all: docker usr xdp_loader xdp_stats xdp_router.o

usr:
	git submodule update --init
	cd libbpf/src; make all OBJDIR=.; make install_headers DESTDIR=../../ OBJDIR=.
	@echo -n "libbpf includes are now available in "
	@ls -d ./usr/include/bpf/

docker: Dockerfile
	docker build -t xdp_router .

push: docker
	docker tag xdp_router:latest marcelwiget/xdp_router:latest
	docker push marcelwiget/xdp_router:latest

build:
	docker-compose build

up:
	docker-compose up -d

down:
	docker-compose down

xdp_loader: *.c *.h
	clang -Wall -O2 -Ilibbpf/include/uapi -Iusr/include \
	  -Llibbpf/src/ -lz -lelf -o xdp_loader \
	  xdp_loader.c common_params.c common_user_bpf_xdp.c libbpf/src/libbpf.a
	strip xdp_loader

xdp_stats: *.c *.h
	clang -Wall -O2 -Ilibbpf/include/uapi -Iusr/include \
	  -Llibbpf/src/ -lz -lelf -o xdp_stats \
	xdp_stats.c common_params.c common_user_bpf_xdp.c libbpf/src/libbpf.a
	strip xdp_stats

xdp_router.o: usr *.c *.h
#	clang -Wall -Wno-unused-value -Wno-pointer-sign -Wno-compare-distinct-pointer-types -Werror -emit-llvm -O2 -g -c -target bpf -D __BPF_TRACING__ -Iusr/include -c 
#	llc -march=bpf -filetype=obj -o xdp_router.o xdp_router.ll
	clang -Wall -O2 -g -c -target bpf -Iusr/include -c \
	  xdp_router.c -o xdp_router.o
	llvm-objdump -S xdp_router.o

remove:
	docker exec r2 ip link set dev eth0 xdp off

clean:
	rm -rf ./usr xdp_loader xdp_stats *.o
	-docker rmi xdp-router

install: xdp_router.o
	docker cp xdp_router.o r2:/
	docker exec r2 sh -c 'ulimit -l 1024 && xdp_loader -F -d eth0 --progsec xdp_router'

gre: xdp_router.o
	docker cp xdp_router.o r2:/
	docker exec r2 xdp_loader -U -d eth0
	docker exec r2 sh -c 'ulimit -l 1024 && xdp_loader -F -d eth0 --progsec xdp_gre'

icmp: xdp_router.o
	docker cp xdp_router.o r2:/
	docker exec r2 xdp_loader -U -d eth0
	docker exec r2 sh -c 'ulimit -l 1024 && xdp_loader -F -d eth0 --progsec xdp_icmp_echo'

remove:
	docker exec r2 xdp_loader -U -d eth0

r4: xdp_router.o
	scp xdp_router.o xdp_loader xdp_stats r4:
	ssh r4 sudo mount -t bpf bpf /sys/fs/bpf/
	ssh r4 sudo ./xdp_loader -F -d enp2s0 --progsec xdp_icmp_echo
#	ssh r4 sudo ./xdp_loader -F -d enp3s0 --progsec xdp_gre
