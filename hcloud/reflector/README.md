## Hetzner Cloud 

Bring up 2 cRPD instances, interconnected via private network, via Terraform.

Adding junos config not yet present.

Please create terraform.tfvars with your private content:

```
$ cat terraform.tfvars 
hcloud_token = "<your hetzner cloud api token>"
crpd_image = "https://<your-server>/path/junos-routing-crpd-docker-20.1R1.11.tgz"
crpd_license = "https://<your-server>/path/junos_sfnt.lic"
```

Bring up the instances with

```
terraform init
terraform plan
terraform apply
```

Picked ubuntu 18.04, but its kernel is too old. Need at least 4.20. Ubuntu 20.04 is available too ...

```
Welcome to Ubuntu 18.04.4 LTS (GNU/Linux 4.15.0-99-generic x86_64)

 * Documentation:  https://help.ubuntu.com
  * Management:     https://landscape.canonical.com
   * Support:        https://ubuntu.com/advantage

   root@r1:~# uname -a
   Linux r1 4.15.0-99-generic #100-Ubuntu SMP Wed Apr 22 20:32:56 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
   root@r1:~# lsb_release -a
   No LSB modules are available.
   Distributor ID: Ubuntu
   Description:    Ubuntu 18.04.4 LTS
   Release:        18.04
   Codename:       bionic
   root@r1:~# exit 
   logout

```

Ubuntu 20.04 has a 5.4 kernel, cool:

```
Welcome to Ubuntu 20.04 LTS (GNU/Linux 5.4.0-28-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

root@r1:~# uname -a
Linux r1 5.4.0-28-generic #32-Ubuntu SMP Wed Apr 22 17:40:10 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
```
