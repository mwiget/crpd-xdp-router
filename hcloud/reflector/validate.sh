#!/bin/bash

SECONDS=0

publicip=""
while true; do
  sleep 1
  terraform refresh >/dev/null
  publicip=$(terraform output -json public_ip4_r1 |jq -r .)
  if [ -z "$publicip" ]; then
    echo "$SECONDS: waiting for public ip to be assigned ... "
  else
    break;
  fi
done
echo "${SECONDS}s: anycast ip is $publicip"

while ! ping -c 1 -W 1 $publicip >/dev/null; do
  echo "${SECONDS}s: waiting for anycast ip respond to icmp requests ..."
  sleep 1
done
echo "${SECONDS}s: anycast ip is reachable"

while true; do
  crpdversion=$(ssh -o StrictHostKeyChecking=no root@$publicip docker exec crpd cli show version 2>/dev/null|grep version)
  if [ -z "$crpdversion" ]; then
    echo "$SECONDS: waiting for cRPD to be installed and ready for service ..."
    sleep 5
  else
    break;
  fi
done
echo "${SECONDS}s: $crpdversion"

exit


# this example checks netconf access to verify cRPD via port 830
#while true; do
#  crpdversion=$(echo "<rpc><get-software-information/></rpc>" | ssh -o StrictHostKeyChecking=no $publicip  -p 830 -s netconf 2>/dev/null |grep 'builder')
#  if [ -z "$crpdversion" ]; then
#    echo "$SECONDS: waiting for cRPD to be installed and ready for service ..."
#    sleep 5
#  else
#    break;
#  fi
#done
#echo "${SECONDS}s: cRPD ready with $crpdversion"

exit
