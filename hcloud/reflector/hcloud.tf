variable "hcloud_token" {}
variable "crpd_image" {}
variable "crpd_license" {}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = var.hcloud_token
}

#  Main ssh key
resource "hcloud_ssh_key" "default" {
  name       = "main ssh key"
  public_key = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAbppm0i41M7zMFba3EWdtuxCaomRQzEdonIzXSiETsZ mwiget@may2020"
}

# Create a VM with cRPD
resource "hcloud_server" "r1" {
  name = "r1"
  image = "ubuntu-20.04"
  server_type = "ccx11"
  ssh_keys    = ["${hcloud_ssh_key.default.name}"]
  user_data =  <<EOT
#cloud-config

package_udpate: true
package_upgrade: true
package_reboot_if_required: true

packages:
  - apt-transport-https
  - ca-certificates
  - curl
  - software-properties-common

write_files:
  - path: /etc/ssh/sshd_config
    content: |
      Port 22
      Port 830
      Subsystem netconf docker exec -i crpd /usr/bin/netconf
    append: true
  - path: /root/crpdconfig/juniper.conf
    content: |
      policy-options {
          policy-statement accept-all {
              then accept;
          }
          policy-statement deny-all {
              then reject;
          }
      }
      routing-options {
          router-id 10.0.0.0;
          autonomous-system 65000.65000;
      }
      protocols {
          bgp {
              family inet {
                  unicast;
              }
              family inet6 {
                  unicast {
                      nexthop-resolution {
                          no-resolution;
                      }
                      no-install;
                  }
              }
              group internal {
                  type internal;
                  multihop;
                  import accept-all;
                  export accept-all;
                  allow 0.0.0.0/0;
              }
              connect-retry-interval 1;
          }
      }


runcmd:
  - curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  - add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  - apt update -y
  - apt install -y docker-ce docker-ce-cli containerd.io
  - systemctl start docker
  - systemctl enable docker
  - wget -O- ${var.crpd_image} | docker load
  - mkdir -p /root/crpdconfig/license/safenet
  - wget -O/root/crpdconfig/license/safenet/junos_sfnt.lic ${var.crpd_license}
  - docker run -ti --rm --name crpd --privileged -v /root/crpdconfig:/config --net host -d crpd:20.1R1.11

final_message: "The system is finally up, after $UPTIME seconds"
EOT
}
resource "hcloud_server" "r2" {
  name = "r2"
  image = "ubuntu-20.04"
  server_type = "ccx11"
  ssh_keys    = ["${hcloud_ssh_key.default.name}"]
  user_data =  <<EOT
#cloud-config

package_udpate: true
package_upgrade: true
package_reboot_if_required: true

packages:
  - apt-transport-https
  - ca-certificates
  - curl
  - software-properties-common

write_files:
  - path: /etc/ssh/sshd_config
    content: |
      Port 22
      Port 830
      Subsystem netconf docker exec -i crpd /usr/bin/netconf
    append: true
  - path: /root/crpdconfig/juniper.conf
    content: |
      policy-options {
          policy-statement accept-all {
              then accept;
          }
          policy-statement deny-all {
              then reject;
          }
      }
      routing-options {
          router-id 10.0.0.0;
          autonomous-system 65000.65000;
      }
      protocols {
          bgp {
              family inet {
                  unicast;
              }
              family inet6 {
                  unicast {
                      nexthop-resolution {
                          no-resolution;
                      }
                      no-install;
                  }
              }
              group internal {
                  type internal;
                  multihop;
                  import accept-all;
                  export accept-all;
                  allow 0.0.0.0/0;
              }
              connect-retry-interval 1;
          }
      }

runcmd:
  - curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  - add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  - apt update -y
  - apt install -y docker-ce docker-ce-cli containerd.io
  - systemctl start docker
  - systemctl enable docker
  - wget -O- ${var.crpd_image} | docker load
  - mkdir -p /root/crpdconfig/license/safenet
  - wget -O/root/crpdconfig/license/safenet/junos_sfnt.lic ${var.crpd_license}
  - docker run -ti --rm --name crpd --privileged -v /root/crpdconfig:/config --net host -d crpd:20.1R1.11

final_message: "The system is finally up, after $UPTIME seconds"
EOT
}

resource "hcloud_network" "mynet1" {
  name = "my-net1"
  ip_range = "10.10.0.0/16"
}
resource "hcloud_network_subnet" "foonet1" {
  network_id = hcloud_network.mynet1.id
  type = "server"
  network_zone = "eu-central"
  ip_range   = "10.10.0.0/24"
}
resource "hcloud_server_network" "srvnetwork-r1" {
  server_id = hcloud_server.r2.id
  network_id = hcloud_network.mynet1.id
  ip = "10.10.0.2"
}
resource "hcloud_server_network" "srvnetwork-r2" {
  server_id = hcloud_server.r2.id
  network_id = hcloud_network.mynet1.id
  ip = "10.10.0.2"
}
output "public_ip4_r1" {
  value = "${hcloud_server.r1.ipv4_address}"
}
output "public_ip4_r2" {
  value = "${hcloud_server.r2.ipv4_address}"
}
