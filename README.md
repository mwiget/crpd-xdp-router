# crpd-xpd-router

This repo contains my workbench exploring XDP and cRPD on baremetal and public clouds. 

Heavily inspired by https://github.com/xdp-project/xdp-tutorial and the blog post
[Building an XDP (eXpress Data Path) based BGP peering router](https://medium.com/swlh/building-a-xdp-express-data-path-based-peering-router-20db4995da66).

Use 'git clone --recursive' to get the submodule libbpf updated as well:

[libbpf standalone lib as submodule](https://github.com/libbpf/libbpf) used to build
XDP apps without kernel (source) dependencies and covered under a dual license: LGPL-2.1 OR BSD-2-Clause.

## Content

- [dc/](dc/) R1 and R2 route GRE over UDP traffic based on routes learned via BGP from 3 clients
- [crpd_trafgen/](crpd_trafgen/) Build cRPD container with traffig generation via scapy for low speed and trafgen for high volume
- [sp/](sp/) R1 and R2 route GRE over UDP traffic based on routes learned via BGP from 3 client containers, deployed in SP//. 
- [xdp_router.c](xdp_router.c) contains the XDP code used to route GRE over UDP packets based on the inner header destination IP address using XDP_TX on single stick and XDP_REDIRECT using more than 1 interface (wip). This code is a modified version from the xdp-tutorial mentioned earler.
- [macvlan/](macvlan/) contains a lab setup using a physical loopback cable between 10GE ports, blasting traffic between R1 and client c1, routes advertised via BGP between them. 
- [xdp-drop-test/](xdp-drop-test/) contains my very early steps in XDP and archived in here.


## References

- [Building an XDP (eXpress Data Path) based BGP peering router](https://medium.com/swlh/building-a-xdp-express-data-path-based-peering-router-20db4995da66).
- [BPF and XDP Reference Guide](https://docs.cilium.io/en/v1.6/bpf/#bpf-and-xdp-reference-guide)
- [Load XDP programs using the ip (iproute2) command](https://link.medium.com/LNpGdu4td3)
- [AF_XDP kernel doc](https://www.kernel.org/doc/html/v5.3/networking/af_xdp.html)
- [xdp-tutorial](https://github.com/xdp-project/xdp-tutorial)
- [Fast Packet Processing with eBPF and XDP: Concepts, Code, Challenges, and Applications](https://www.researchgate.net/publication/339084847_Fast_Packet_Processing_with_eBPF_and_XDP_Concepts_Code_Challenges_and_Applications)
- [Apache-licensed library for executing eBPF programs](https://github.com/iovisor/ubpf)
- [The eXpress data path: fast programmable packet processing in the operating system kernel](https://dl.acm.org/doi/10.1145/3281411.3281443)
- [The power of XDP, blog post with xdping example](https://blogs.oracle.com/linux/the-power-of-xdp)
- [A practical introduction to XDP, LPC Vancouver 2018](https://linuxplumbersconf.org/event/2/contributions/71/attachments/17/9/presentation-lpc2018-xdp-tutorial.pdf)
- [Creating Complex Network Services with eBPF: Experience and Lessons Learned](https://sebymiano.github.io/documents/18-eBPF-experience.pdf)
- [Netdev 0x13 talk by Toshiaki Makita on Veth XDP](https://youtu.be/q3gjNe6LKDI)
- [Achieving high-performance, low-latency networking with XDP: Part I](https://developers.redhat.com/blog/2018/12/06/achieving-high-performance-low-latency-networking-with-xdp-part-1/)
- [Using eXpress Data Path (XDP) maps in RHEL 8: Part 2](https://developers.redhat.com/blog/2018/12/17/using-xdp-maps-rhel8/)

## Notes May 31st, 2020

- Fedora on apu with kernel 5.6.10-300.fc32.x86_64 is no good for XDP. Can't load any of my objects:

```
mwiget@r4 ~]$ sudo mount -t bpf bpf /sys/fs/bpf
mwiget@r4 ~]$ sudo ./xdp_loader -d enp3s0 --progsec xdp_pass -F -A
libbpf: map 'tx_port': failed to create: Invalid argument(-22)
libbpf: failed to load object 'xdp_router.o'
ERR: loading BPF-OBJ file(xdp_router.o) (-22): Invalid argument
ERR: loading file: xdp_router.o
```

- looks like XDP_TX and XDP_REDIRECT don't work (for me) in docker containers (veth pairs). Maybe I need to
 add an xdp_pass on the other end of the veth link, but thats not straightforward.
- `sudo ./xdp_loader -d enp6s0 --progsec xdp_gre --auto-mode -F` on xeon worked with commit 'xdp_icmp_echo and xdp_gre work when used on enp6s0 in auto-mode' on May 31st.
- Same for `sudo ./xdp_loader -d enp6s0 --progsec xdp_gre --auto-mode -F`, after adding proper routes for the packet I sent to xeon from ipxe:

```
pi@ipxe:~ $ ping 10.0.0.1
PING 10.0.0.1 (10.0.0.1) 56(84) bytes of data.
^[[A^C
--- 10.0.0.1 ping statistics ---
225 packets transmitted, 0 received, 100% packet loss, time 1044ms
```

```
mwiget@xeon:~/crpd-xdp-router$ ip r
default via 192.168.0.1 dev enp6s0 proto dhcp metric 101
10.0.0.1 via 192.168.0.51 dev enp6s0
```

Then I see the packets returned to ipxe:

```
i@ipxe:~ $ sudo tcpdump -n -i eth0 ip -e
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes
17:24:49.426856 b8:27:eb:f1:05:a1 > 90:1b:0e:9b:3a:5c, ethertype IPv4 (0x0800), length 122: 192.168.0.51 > 192.168.0.50: GREv0, proto IPv4 (0x0800), length 88: 10.0.1.2 > 10.0.0.1: ICMP echo request, id 32755, seq 157, length
64
17:24:49.427398 90:1b:0e:9b:3a:5c > b8:27:eb:f1:05:a1, ethertype IPv4 (0x0800), length 122: 192.168.0.51 > 192.168.0.51: GREv0, proto IPv4 (0x0800), length 88: 10.0.1.2 > 10.0.0.1: ICMP echo request, id 32755, seq 157, length
64
17:24:50.466874 b8:27:eb:f1:05:a1 > 90:1b:0e:9b:3a:5c, ethertype IPv4 (0x0800), length 122: 192.168.0.51 > 192.168.0.50: GREv0, proto IPv4 (0x0800), length 88: 10.0.1.2 > 10.0.0.1: ICMP echo request, id 32755, seq 158, length
64
```

## Update on container with XDP_TX

xdp_icmp and xdp_gre using XDP_TX works now. Had to load xdp_pass app on all veth endpoints outside the containers. Created a shell script doing so, run right after 'docker-comose up -d': [veth-xdp-loader.sh](veth-xdp-loader.sh).

```
XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.000330
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.000330
XDP_PASS               5 pkts (         1 pps)           0 Kbytes (     0 Mbits/s) period:2.000330
XDP_TX                81 pkts (         2 pps)           9 Kbytes (     0 Mbits/s) period:2.000330
XDP_REDIRECT           0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.000330
```




```
        0x0090:  0000 0000 0000                           ......
21:07:15.550509 02:42:0a:0a:0a:0b > 02:42:0a:0a:0a:0c, ethertype IPv4 (0x0800), length 122: (tos 0x0, ttl 255, id 7744, offset 0, flags [DF], proto GRE (47), length 108)
    10.10.10.11 > 10.10.10.12: GREv0, Flags [none], proto IPv4 (0x0800), length 88
        (tos 0x0, ttl 64, id 56971, offset 0, flags [DF], proto ICMP (1), length 84)
    10.0.0.1 > 10.0.0.3: ICMP echo request, id 9984, seq 21, length 64
        0x0000:  0242 0a0a 0a0c 0242 0a0a 0a0b 0800 4500  .B.....B......E.
        0x0010:  006c 1e40 4000 ff2f 34f8 0a0a 0a0b 0a0a  .l.@@../4.......
        0x0020:  0a0c 0000 0800 4500 0054 de8b 4000 4001  ......E..T..@.@.
        0x0030:  481a 0a00 0001 0a00 0003 0800 4645 2700  H...........FE'.
        0x0040:  0015 d046 ba5e 0000 0000 0000 0000 0000  ...F.^..........
        0x0050:  0000 0000 0000 0000 0000 0000 0000 0000  ................
        0x0060:  0000 0000 0000 0000 0000 0000 0000 0000  ................
        0x0070:  0000 0000 0000 0000 0000                 ..........
21:07:16.303286 02:42:0a:0a:0a:0b > 02:42:0a:0a:0a:0c, ethertype IPv4 (0x0800), length 122: (tos 0x0, ttl 255, id 7884, offset 0, flags [DF], proto GRE (47), length 108)
    10.10.10.11 > 10.10.10.12: GREv0, Flags [none], proto IPv4 (0x0800), length 88
        (tos 0x0, ttl 64, id 57086, offset 0, flags [DF], proto ICMP (1), length 84)
    10.0.0.1 > 10.0.0.3: ICMP echo request, id 3584, seq 240, length 64
        0x0000:  0242 0a0a 0a0c 0242 0a0a 0a0b 0800 4500  .B.....B......E.
        0x0010:  006c 1ecc 4000 ff2f 346c 0a0a 0a0b 0a0a  .l..@../4l......
        0x0020:  0a0c 0000 0800 4500 0054 defe 4000 4001  ......E..T..@.@.
        0x0030:  47a7 0a00 0001 0a00 0003 0800 c9ed 0e00  G...............
        0x0040:  00f0 59c3 c55e 0000 0000 0000 0000 0000  ..Y..^..........
        0x0050:  0000 0000 0000 0000 0000 0000 0000 0000  ................
        0x0060:  0000 0000 0000 0000 0000 0000 0000 0000  ................
        0x0070:  0000 0000 0000 0000 0000                 ..........
21:07:16.303349 02:42:0a:0a:0a:0d > 02:42:0a:0a:0a:0b, ethertype IPv4 (0x0800), length 150: (tos 0xc0, ttl 64, id 24328, offset 0, flags [none], proto ICMP (1), length 136)
    10.10.10.13 > 10.10.10.11: ICMP 10.10.10.13 protocol 47 port 2048 unreachable, length 116
        (tos 0x0, ttl 254, id 7884, offset 0, flags [DF], proto GRE (47), length 108)
    10.10.10.11 > 10.10.10.13: GREv0, Flags [none], proto IPv4 (0x0800), length 88
        (tos 0x0, ttl 64, id 57086, offset 0, flags [DF], proto ICMP (1), length 84)
    10.0.0.1 > 10.0.0.3: ICMP echo request, id 3584, seq 240, length 64
        0x0000:  0242 0a0a 0a0b 0242 0a0a 0a0d 0800 45c0  .B.....B......E.
        0x0010:  0088 5f08 0000 4001 f281 0a0a 0a0d 0a0a  .._...@.........
        0x0020:  0a0b 0303 f4fc 0000 0000 4500 006c 1ecc  ..........E..l..
        0x0030:  4000 fe2f 356b 0a0a 0a0b 0a0a 0a0d 0000  @../5k..........
        0x0040:  0800 4500 0054 defe 4000 4001 47a7 0a00  ..E..T..@.@.G...
        0x0050:  0001 0a00 0003 0800 c9ed 0e00 00f0 59c3  ..............Y.
        0x0060:  c55e 0000 0000 0000 0000 0000 0000 0000  .^..............
        0x0070:  0000 0000 0000 0000 0000 0000 0000 0000  ................
        0x0080:  0000 0000 0000 0000 0000 0000 0000 0000  ................
        0x0090:  0000 0000 0000
```

the packets are reaching r3 and the response goes back. There is something broken with the response, it gets encapsulated again when it reaches r1.

```
mwiget@xeon:~/crpd-xdp-router$ docker exec -ti r3 tcpdump -n -i eth0 -e
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on eth0, link-type EN10MB (Ethernet), capture size 262144 bytes
21:15:35.402407 02:42:0a:0a:0a:0c > 02:42:0a:0a:0a:0d, ethertype IPv4 (0x0800), length 122: 10.10.10.11 > 10.10.10.13: GREv0, proto IPv4 (0x0800), length 88: 10.0.0.1 > 10.0.0.3: ICMP echo request, id 3584, seq 739, length 64
21:15:35.402441 02:42:0a:0a:0a:0d > 02:42:0a:0a:0a:0b, ethertype IPv4 (0x0800), length 150: 10.10.10.13 > 10.10.10.11: ICMP 10.10.10.13 protocol 47 port 2048 unreachable, length 116
21:15:35.631601 02:42:0a:0a:0a:0c > 02:42:0a:0a:0a:0d, ethertype IPv4 (0x0800), length 122: 10.10.10.11 > 10.10.10.13: GREv0, proto IPv4 (0x0800), length 88: 10.0.0.1 > 10.0.0.3: ICMP echo request, id 9984, seq 521, length 64
21:15:36.402681 02:42:0a:0a:0a:0c > 02:42:0a:0a:0a:0d, ethertype IPv4 (0x0800), length 122: 10.10.10.11 > 10.10.10.13: GREv0, proto IPv4 (0x0800), length 88: 10.0.0.1 > 10.0.0.3: ICMP echo request, id 3584, seq 740, length 64
21:15:36.402717 02:42:0a:0a:0a:0d > 02:42:0a:0a:0a:0b, ethertype IPv4 (0x0800), length 150: 10.10.10.13 > 10.10.10.11: ICMP 10.10.10.13 protocol 47 port 2048 unreachable, length 116
21:15:36.631758 02:42:0a:0a:0a:0c > 02:42:0a:0a:0a:0d, ethertype IPv4 (0x0800), length 122: 10.10.10.11 > 10.10.10.13: GREv0, proto IPv4 (0x0800), length 88: 10.0.0.1 > 10.0.0.3: ICMP echo request, id 9984, seq 522, length 64
21:15:37.402915 02:42:0a:0a:0a:0c > 02:42:0a:0a:0a:0d, ethertype IPv4 (0x0800), length 122: 10.10.10.11 > 10.10.10.13: GREv0, proto IPv4 (0x0800), length 88: 10.0.0.1 > 10.0.0.3: ICMP echo request, id 3584, seq 741, length 64
21:15:37.402942 02:42:0a:0a:0a:0d > 02:42:0a:0a:0a:0b, ethertype IPv4 (0x0800), length 150: 10.10.10.13 > 10.10.10.11: ICMP 10.10.10.13 protocol 47 port 2048 unreachable, length 116
21:15:37.631934 02:42:0a:0a:0a:0c > 02:42:0a:0a:0a:0d, ethertype IPv4 (0x0800), length 122: 10.10.10.11 > 10.10.10.13: GREv0, proto IPv4 (0x0800), length 88: 10.0.0.1 > 10.0.0.3: ICMP echo request, id 9984, seq 523, length 64
^C
9 packets captured
9 packets received by filter
0 packets dropped by kernel
```

... TX_REDIRECT still doesn't work though within a Container ...

Running on Ubuntu 20.04, Linux xeon 5.4.0-33-generic #37-Ubuntu SMP Thu May 21 12:53:59 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux.

