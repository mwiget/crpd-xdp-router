FROM alpine:3.12 as build

# need iproute2 from edge with libelf support to load XDP objects
RUN apk add --no-cache \
  clang-dev git make libbpf-dev llvm linux-headers musl-dev gcc binutils

COPY *.[ch] /

RUN clang -Wall -O2 -Llibbpf/src/ -lz -lelf -o xdp_loader \
   xdp_loader.c common_params.c common_user_bpf_xdp.c /usr/lib/libbpf.a 

RUN clang -Wall -O2 -lz -lelf -o xdp_stats \
   xdp_stats.c common_params.c common_user_bpf_xdp.c /usr/lib/libbpf.a \
   && strip xdp_loader xdp_stats

RUN clang -Wall -O2 -g -c -target bpf -Iusr/include -c -o xdp_router.o \
  xdp_router.c \
  && llvm-objdump -S xdp_router.o

FROM alpine:3.12
# using own xdp_loader, so no
# need for iproute2 from edge with libelf support to load XDP objects
RUN apk add --no-cache tcpdump bwm-ng libelf
#  && apk add iproute2 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main

COPY --from=build xdp_loader xdp_stats /usr/local/bin/
COPY --from=build xdp_router.o /
COPY entrypoint.sh /
ENTRYPOINT ["/bin/sh", "/entrypoint.sh"]
