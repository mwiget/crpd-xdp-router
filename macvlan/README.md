# back2back test via physical loopback cable

```mermaid
graph LR
client1 ---|ens3f1 -- ens3f0| r1
```

client1 is running cRPD and lowspeed trafgen (1pps) in a container using macvlan.
R1 runs cRPD in another container plus a XDP container, both in host mode.

Launch via 'make up':

```
$ docker-compose ps
 Name           Command           State                                Ports                              
 ----------------------------------------------------------------------------------------------------------
 c1       /sbin/runit-init.sh      Up      179/tcp, 22/tcp, 3784/tcp, 4784/tcp, 6784/tcp, 7784/tcp, 830/tcp
 r1crpd   /sbin/runit-init.sh      Up                                                                      
 r1xdp    /bin/sh /entrypoint.sh   Up           
 ```

To blast traffic, the gre.cfg and site.h file from the client1 container are downloaded to the host and trafgen is run locally using ens3f1:

```
docker cp c1:/root/gre.cfg .
docker cp c1:/root/site.h .
sudo trafgen -o ens3f1 -p --conf gre.cfg --cpus 4
```

Or simply run 'make blast'.

This generates roughly 3.6Mpps:

```
$ make stats

XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.000300
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.000299
XDP_PASS               7 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.000299
XDP_TX        3834358219 pkts (   3639477 pps)   402607612 Kbytes (  3057 Mbits/s) period:2.000299
XDP_REDIRECT           0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.000299

XDP_ABORTED            0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.000304
XDP_DROP               0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.000308
XDP_PASS               7 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.000308
XDP_TX        3841595341 pkts (   3618005 pps)   403367510 Kbytes (  3039 Mbits/s) period:2.000308
XDP_REDIRECT           0 pkts (         0 pps)           0 Kbytes (     0 Mbits/s) period:2.000308
```

```
$ make bwm

bwm-ng v0.6.2 (probing every 0.500s), press 'h' for help
input: /proc/net/dev type: rate
\         iface                   Rx                   Tx                Total
==============================================================================
mpqemubr0:            0.00 P/s             0.00 P/s             0.00 P/s
lsi:            0.00 P/s             0.00 P/s             0.00 P/s
ens3f0:      3644812.75 P/s       3644812.75 P/s       7289625.50 P/s
docker0:            0.00 P/s             0.00 P/s             0.00 P/s
enp0s25:            0.00 P/s             0.00 P/s             0.00 P/s
lo:            0.00 P/s             0.00 P/s             0.00 P/s
ens3f1:            0.00 P/s       3645567.75 P/s       3645567.75 P/s
enp6s0:            1.99 P/s             1.99 P/s             3.98 P/s
vethc359591:            0.00 P/s             0.00 P/s             0.00 P/s
br-bafe47251063:            0.00 P/s             0.00 P/s             0.00 P/s
------------------------------------------------------------------------------
total:      3644814.75 P/s       7290382.50 P/s      10935198.00 P/s
```


The generated packet:

```
$ sudo tcpdump -n -i ens3f1 -vXe
tcpdump: listening on ens3f1, link-type EN10MB (Ethernet), capture size 262144 bytes
17:21:05.537529 02:42:41:62:f9:aa > 02:42:0a:0a:0a:0b, ethertype IPv4 (0x0800), length 105: (tos 0x0, ttl 255, id 0, offset 0, flags [none], proto UDP (17), length 91)
    10.0.0.1.26690 > 10.10.10.11.4754: UDP, length 63   
        0x0000:  4500 005b 0000 0000 ff11 9d7c 0a00 0001  E..[.......|....                                       
        0x0010:  0a0a 0a0b 6842 1292 0047 0000 0000 0800  ....hB...G......
        0x0020:  4500 003a b575 4000 4011 566f c0a8 0164  E..:.u@.@.Vo...d
        0x0030:  0a0b 62b7 0007 0007 0014 0000 6372 7064  ..b.........crpd                                       
        0x0040:  2d78 6470 2d72 6f75 7465 7220 7472 6166  -xdp-router.traf
        0x0050:  6765 6e20 626c 6173 7465 72              gen.blaster
```
