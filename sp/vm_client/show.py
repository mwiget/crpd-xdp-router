from jnpr.junos import Device
from jnpr.junos.op.routes import RouteTable
from pprint import pprint
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-t', '--target', dest='target', default='localhost')
parser.add_argument('-u', '--user', dest='username', default='')
parser.add_argument('-p', '--pass', dest='password', default='')
parser.add_argument('-P', '--port', dest='port', default=830)
args = parser.parse_args()

print("get facts using ssh keys ...")
with Device(host=args.target, username=args.username, password=args.password, port=args.port) as dev:
    pprint(dev.facts)

    print()
    print("routing table:")
    tbl = RouteTable(dev)
    tbl.get()
    for item in tbl:
        print(item.key, item.protocol, "via", item.via)
