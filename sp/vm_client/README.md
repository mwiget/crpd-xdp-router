# Client cRPD on SP// within Ubuntu VMs

Prepare secrets variable file with your SP// and cRPD image and license file URLs:

```
$ cat terraform.tfvars
stackpath_stack = "crpd-srv6-perf-61d4cc"
stackpath_client_id = "<stackpath_client_id>"
stackpath_client_secret = "<stackpath_client_secret>"
crpd_image = "https://<your-web-server>/path/junos-routing-crpd-docker-20.1R1.11.tgz"
crpd_license = "https://<your-web-server>/path/junos_sfnt.lic"
```

(I'm using Dropbox generated sharelinks to feed the cRPD image and license file)

Launch cRPD within Ubuntu VMs on SP// via Terraform:

```
$ terraform apply -auto-approve
```

Takes about 15 minutes using 1 core and 2GB of RAM VMs. Once up and running, you can access the cRPD CLI

```
$ ssh -t 151.139.87.51 docker exec -ti crpd cli show version
Hostname: crpd-eu-fra-0
Model: cRPD
cRPD package version : 20.1R1.11 built by builder on 2020-03-20 18:26:21 UTC
Connection to 151.139.87.51 closed.
```

Ethernet interface provided within VM:

```
mwiget@client-eu-fra-0:~$ lspci |grep Eth
04:00.0 Ethernet controller: Cavium, Inc. CN23XX [LiquidIO II] SRIOV Virtual Function (rev 03)
mwiget@client-eu-fra-0:~$
```
