#!/bin/bash
terraform refresh >/dev/null
terraform output -json instances |jq -r 'to_entries[] | [.value.public_ip, .key] | @tsv'
