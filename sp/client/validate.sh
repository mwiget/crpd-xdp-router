#!/bin/bash

SECONDS=0

instanceip=""
while true; do
  sleep 1
  terraform refresh >/dev/null
  instanceip=$(terraform output -json instances |jq -r 'to_entries[] | [.value.public_ip, .key] | @tsv'|head -1|cut -f1)
  if [ -z "$instanceip" ]; then
    echo "$SECONDS: waiting for instance ip to be assigned ... "
  else
    break;
  fi
done
echo "${SECONDS}s: instance ip is $anycastip"

while ! ping -c 1 -W 1 $instanceip >/dev/null; do
  echo "${SECONDS}s: waiting for instance ip respond to icmp requests ..."
  sleep 1
done
echo "${SECONDS}s: instance ip is reachable"

while true; do
  crpdversion=$(ssh -o StrictHostKeyChecking=no $instanceip docker exec crpd cli show version 2>/dev/null|grep version)
  if [ -z "$crpdversion" ]; then
    echo "$SECONDS: waiting for cRPD to be installed and ready for service ..."
    sleep 5
  else
    break;
  fi
done
echo "${SECONDS}s: $crpdversion"

exit


# this example checks netconf access to verify cRPD via port 830
#while true; do
#  crpdversion=$(echo "<rpc><get-software-information/></rpc>" | ssh -o StrictHostKeyChecking=no $instanceip  -p 830 -s netconf 2>/dev/null |grep 'builder')
#  if [ -z "$crpdversion" ]; then
#    echo "$SECONDS: waiting for cRPD to be installed and ready for service ..."
#    sleep 5
#  else
#    break;
#  fi
#done
#echo "${SECONDS}s: cRPD ready with $crpdversion"

exit
