# Client cRPD on SP// within Ubuntu VMs

Prepare secrets variable file with your SP// and cRPD image and license file URLs:

```
$ cat terraform.tfvars
$ cat terraform.tfvars                      
stackpath_stack = "crpd-srv6-perf-61d4cc"
stackpath_client_id = "<stackpath_client_id>"
stackpath_client_secret = "<stackpath_client_secret>"
credentials_username = "<docker registry username>"                                               
credentials_password = "<docker registry password"       
```

Launch cRPD as container with a built-in trafgen (1 pps):

```
$ terraform apply -auto-approve
```

Right now, the ssh public key is baked into the image. The image can be built form the toplevel folder crpd_trafgen in this repo,
pushed and used instead.

Another note: There are 3 env variables that need to be set, which point to the IP addresses of R1 and R2:


- R1, R2: IP addresses of the remote BGP, internal IP's
- GRE: tunnel endpoint for the GRE traffic generated from this instance.

An example using all containers deployed via docker-compose can be found in the toplevel folder dc of this repo.


While I got the clients up and connected via BGP to the reflectors, I can't use trafgen due 
to the restricted kata privileges:

```
root@client-eu-fra-0:~# trafgen -o gre.pcap -p --conf gre.cfg --no-sock-mem -t 1s -n 3
Cannot lock pages!          
```


