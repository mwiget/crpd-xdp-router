#!/bin/bash
for IP in $(terraform output -json instances |jq -r 'to_entries[] | [.value.public_ip] | @tsv')
do
  NAME=$(ssh -o StrictHostKeyChecking=no $IP hostname)
  echo "$NAME ($IP) ..."
  mkdir -p crpdconfig
  ssh -o StrictHostKeyChecking=no $IP docker exec crpd cli show configuration > crpdconfig/$NAME.conf.txt
  ls -l crpdconfig/$NAME.conf.txt
done

