variable "stackpath_stack" {}
variable "stackpath_client_id" {}
variable "stackpath_client_secret" {}
variable "crpd_image" {}
variable "crpd_license" {}

provider "stackpath" {
  stack_id      = var.stackpath_stack
  client_id     = var.stackpath_client_id
  client_secret = var.stackpath_client_secret
}

resource "stackpath_compute_workload" "reflector" {
  name = "reflector"
  slug = "reflector"

  annotations = {
   # don't request an anycast IP
  "anycast.platform.stackpath.net" = "true"
  }

  network_interface {
   network = "default"
  }

  virtual_machine {

    name = "r"
    image = "stackpath-edge/ubuntu-1804-bionic:v201909061930"

    resources {
      requests = {
        "cpu" = "1"
        "memory" = "2Gi"
      }
    }

    # The ports that should be publicly exposed on the VM.
    port {
      name = "ssh"
      port = 22
      protocol = "TCP"
      enable_implicit_network_policy = true
    }
    port {
      name = "bgp"
      port = 179
      protocol = "TCP"
      enable_implicit_network_policy = true
    }
    port {
      name = "netconf"
      port = 830
      protocol = "TCP"
      enable_implicit_network_policy = true
    }
    port {
      name = "gre-over-udp"
      port = 4754
      protocol = "UDP"
      enable_implicit_network_policy = true
    }

    user_data = <<EOT
#cloud-config
ssh_authorized_keys:
  - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAbppm0i41M7zMFba3EWdtuxCaomRQzEdonIzXSiETsZ mwiget@may2020

package_update: true
package_upgrade: true
package_reboot_if_required: true

groups:
- docker

users:
- default
- name: mwiget
  lock_passwd: true
  shell: /bin/bash
  ssh-authorized_keys:
    - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAbppm0i41M7zMFba3EWdtuxCaomRQzEdonIzXSiETsZ mwiget@may20202
    - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDr1zH+NmWzf/+qtCTqC/+QAHoWIoq3k3YjH/IsjYdHXZ0mQonsMlrL+owArvLtvi3gXxqPGlO/aWt53v8KAY+RV7IOSbqfFY56k0GTmvPJisSsBkAedruu05hqlFMS/2mkNFL/BsWNzL617LtuFQpN6ud57QSrQruQQtIKTuWUe+XjqkSNiAkvD4zc4tip9ovULhC9QY/IVmhguVDJ0FuQWCDd4l7IM+KjlTXGplN5Y9bIVuU+nnSHnUEkRFxuGX1pvOHB1L31INlD9CVJHDA6bBJyIQgv0WcqoA2/3/8eRqN/pXOe+clglJGRT6bb/+5Sfy6JZoA0OlsyW66VfGR3 mwiget@xeon
  groups:
    - docker
    - sudo
  sudo:
    - ALL=(ALL) NOPASSWD:ALL

write_files:
  - path: /etc/ssh/sshd_config
    append: true
    content: |
      Port 22
      Port 830
      Subsystem netconf docker exec -i crpd /usr/bin/netconf
  - path: /root/crpdconfig/juniper.conf
    content: |
      policy-options {
          policy-statement accept-all {
              then accept;
          }
          policy-statement deny-all {
              then reject;
          }
      }
      routing-options {
          router-id 10.0.0.0;
          autonomous-system 65000.65000;
      }
      protocols {
          bgp {
              family inet {
                  unicast;
              }
              family inet6 {
                  unicast;
              }
              group internal {
                  type internal;
                  multihop;
                  import accept-all;
                  export accept-all;
                  allow 0.0.0.0/0;
              }
              connect-retry-interval 1;
          }
      }

packages:
  - apt-transport-https
  - ca-certificates
  - curl
  - gnupg-agent
  - software-properties-common
  - linux-generic-hwe-18.04

runcmd:
  - curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
  - add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  - apt-get update -y
  - apt-get install -y docker-ce docker-ce-cli containerd.io
  - systemctl start docker
  - systemctl enable docker
  - wget -O- ${var.crpd_image} | docker load
  - mkdir -p /root/crpdconfig/license/safenet
  - wget -O/root/crpdconfig/license/safenet/junos_sfnt.lic ${var.crpd_license}
  - docker run -ti --name xdp_router --privileged --env INTERFACE=eth0 --restart always --net host -d marcelwiget/xdp_router
  - docker run -ti --name crpd --privileged --restart always -v /root/crpdconfig:/config --net host -d crpd:20.1R1.11

final_message: "The system is finally up, after $UPTIME seconds"
EOT

  }

  target {
    name = "eu"
    min_replicas = 2
    max_replicas = 2
    deployment_scope = "cityCode"
    selector {
      key      = "cityCode"
      operator = "in"
      values   = [
        "FRA"
      ]
    }
  }
}

output "instances" {
  value = {
    for instance in stackpath_compute_workload.reflector.instances :
    instance.name => {
      "name"       = instance.name
      "public_ip"  = instance.external_ip_address
      "private_ip" = instance.ip_address
      "phase"      = instance.phase
    }
  }
}

output "instance_ips" {
  value = ["${stackpath_compute_workload.reflector.instances.*.external_ip_address}"]
}


output "anycast_ip" {
  value = replace(lookup(stackpath_compute_workload.reflector.annotations, "anycast.platform.stackpath.net/subnets", ""), "/32", "")
}

