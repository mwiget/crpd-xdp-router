#!/bin/bash
terraform refresh >/dev/null
anycastip=$(terraform output -json anycast_ip |jq -r .)
echo "$anycastip  reflector"
terraform output -json instances |jq -r 'to_entries[] | [.value.public_ip, .key] | @tsv'
