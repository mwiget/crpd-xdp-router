#!/bin/bash

SECONDS=0

anycastip=""
while true; do
  sleep 1
  terraform refresh >/dev/null
  anycastip=$(terraform output -json anycast_ip |jq -r .)
  if [ -z "$anycastip" ]; then
    echo "$SECONDS: waiting for anycast ip to be assigned ... "
  else
    break;
  fi
done
echo "${SECONDS}s: anycast ip is $anycastip"

while ! ping -c 1 -W 1 $anycastip >/dev/null; do
  echo "${SECONDS}s: waiting for anycast ip respond to icmp requests ..."
  sleep 1
done
echo "${SECONDS}s: anycast ip is reachable"

while true; do
  crpdversion=$(ssh -o StrictHostKeyChecking=no $anycastip docker exec crpd cli show version 2>/dev/null|grep version)
  if [ -z "$crpdversion" ]; then
    echo "$SECONDS: waiting for cRPD to be installed and ready for service ..."
    sleep 5
  else
    break;
  fi
done
echo "${SECONDS}s: $crpdversion"

exit


# this example checks netconf access to verify cRPD via port 830
#while true; do
#  crpdversion=$(echo "<rpc><get-software-information/></rpc>" | ssh -o StrictHostKeyChecking=no $anycastip  -p 830 -s netconf 2>/dev/null |grep 'builder')
#  if [ -z "$crpdversion" ]; then
#    echo "$SECONDS: waiting for cRPD to be installed and ready for service ..."
#    sleep 5
#  else
#    break;
#  fi
#done
#echo "${SECONDS}s: cRPD ready with $crpdversion"

exit
