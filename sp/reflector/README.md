# cRPD on SP// within Ubuntu VMs

Prepare secrets variable file with your SP// and cRPD image and license file URLs:

```
$ cat terraform.tfvars
stackpath_stack = "crpd-srv6-perf-61d4cc"
stackpath_client_id = "<stackpath_client_id>"
stackpath_client_secret = "<stackpath_client_secret>"
crpd_image = "https://<your-web-server>/path/junos-routing-crpd-docker-20.1R1.11.tgz"
crpd_license = "https://<your-web-server>/path/junos_sfnt.lic"
```

(I'm using Dropbox generated sharelinks to feed the cRPD image and license file)

Update the user and ssh public keys in the file [reflector.tf](reflector.tf) to gain access
later via ssh and netconf.

Initialize and launch cRPD in Ubuntu VMs on SP// via Terraform:

```
$ terraform init
$ terraform apply -auto-approve
```

Takes about 15 minutes using 1 core and 2GB of RAM VMs. The script [validate.sh](validate.sh) extracts the IP address of R1 and waits until cRPD is operational:

```
$ ./validate.sh
. . .
009: waiting for cRPD to be installed and ready for service ...
1015: waiting for cRPD to be installed and ready for service ...
1021: waiting for cRPD to be installed and ready for service ...
1028: waiting for cRPD to be installed and ready for service ...
1041s: cRPD package version : 20.1R1.11 built by builder on 2020-03-20 18:26:21 UTC 
```

Once up and running, you can access the cRPD CLI

```
$ ssh -t 151.139.87.51 docker exec -ti crpd cli show version
Hostname: crpd-eu-fra-0
Model: cRPD
cRPD package version : 20.1R1.11 built by builder on 2020-03-20 18:26:21 UTC
Connection to 151.139.87.51 closed.
```

To get the IP addresses, use

```
$ terraform refresh
stackpath_compute_workload.reflector: Refreshing state... [id=eca73321-074b-4f09-8f3d-6b0b52d6e9cf]

Outputs:

anycast_ip = 185.85.196.21
instance_ips = [
  [
    "151.139.87.31",
    "151.139.87.32",
  ],
]
instances = {
  "reflector-eu-fra-0" = {
    "name" = "reflector-eu-fra-0"
    "phase" = "RUNNING"
    "private_ip" = "10.128.0.2"
    "public_ip" = "151.139.87.31"
  }
  "reflector-eu-fra-1" = {
    "name" = "reflector-eu-fra-1"
    "phase" = "RUNNING"
    "private_ip" = "10.128.0.3"
    "public_ip" = "151.139.87.32"
  }
}
```

Or in a format suitable for /etc/hosts:

```
$ ./generate-hosts-file.sh
185.85.196.21  reflector
151.139.87.31	reflector-eu-fra-0
151.139.87.32	reflector-eu-fra-1
```

To show bgp summary within 
## Destroy VMs

```
$ terraform destroy --auto-approve
stackpath_compute_workload.crpd: Refreshing state... [id=d45db416-f0ec-46f5-822f-2b472dcd53d4]
stackpath_compute_workload.crpd: Destroying... [id=d45db416-f0ec-46f5-822f-2b472dcd53d4]
stackpath_compute_workload.crpd: Destruction complete after 1s

Destroy complete! Resources: 1 destroyed.
```

## netconf to crpd when running in host mode

In this setup, each cRPD instance runs in network host mode, hence it doesn't really make sense to have it run its own sshd.
Nevertheless I wanted to use NETCONF to provision them, ideally using the user accounts and ssh keys used by the host.
Well, turns out, that's doable by adding the following lines to the hosts /etc/ssh/sshd_config and restart ssh:

```
$ tail -3 /etc/ssh/sshd_config
Port 22
Port 830
Subsystem netconf docker exec -i crpd /usr/bin/netconf
```

```
$ sudo systemctl restart ssh
```

Running cRPD container: (using the name crpd referenced in the Subsystem command:

```
$ docker ps
CONTAINER ID        IMAGE               COMMAND                 CREATED             STATUS              PORTS               NAMES
b219d399e34a        crpd:20.1R1.11      "/sbin/runit-init.sh"   About an hour ago   Up About an hour                        crpd
```

Lets try it out: First the script, quering the cRPD's facts plus its routing table:

```
$ cat show.py
from jnpr.junos import Device
from jnpr.junos.op.routes import RouteTable
from pprint import pprint

print("get facts using ssh keys ...")
with Device(host='localhost') as dev:
    pprint(dev.facts)

    print()
    print("routing table:")
    tbl = RouteTable(dev)
    tbl.get()
    for item in tbl:
        print(item.key, item.protocol, "via", item.via)
```

```
$ python3 show.py
get facts using ssh keys ...
{'2RE': None,
 'HOME': None,
 'RE0': None,
 'RE1': None,
 'RE_hw_mi': None,
 'current_re': None,
 'domain': None,
 'fqdn': 'xeon',
 'hostname': 'xeon',
 'hostname_info': {'re0': 'xeon'},
 'ifd_style': 'CLASSIC',
 'junos_info': None,
 'master': None,
 'model': 'CRPD',
 'model_info': {'re0': 'CRPD'},
 'personality': None,
 're_info': None,
 're_master': None,
 'serialnumber': None,
 'srx_cluster': None,
 'srx_cluster_id': None,
 'srx_cluster_redundancy_group': None,
 'switch_style': 'NONE',
 'vc_capable': False,
 'vc_fabric': None,
 'vc_master': None,
 'vc_mode': None,
 'version': '0.0I0.0',
 'version_RE0': None,
 'version_RE1': None,
 'version_info': junos.version_info(major=(0, 0), type=I, minor=0, build=0),
 'virtual': None}

routing table:
172.17.0.1/32 Local via None
192.168.0.0/24 Direct via enp6s0
192.168.0.50/32 Local via enp6s0
2a02:168:5f67::/64 Direct via enp6s0
2a02:168:5f67:0:50b7:7707:d87b:9a58/128 Local via enp6s0
2a02:168:5f67:0:6242:24ed:abe6:a3b0/128 Local via enp6s0
2a02:168:5f67:0:98b1:b966:4840:181a/128 Local via enp6s0
fe80::42:22ff:fe2b:4ace/128 Local via None
fe80::21b:21ff:fe99:cc14/128 Local via ens3f0
fe80::b776:69e:25fb:6043/128 Local via enp6s0
fe80::e4e8:38ff:fe97:b026/128 Local via lsi
ff02::2/128 INET6 via None
```

Example pyez script launched remotely against cRPD running in a VM on SP//:

```
$ python3 show.py python3 show.py -t 151.139.87.51
get facts using ssh keys ...
{'2RE': None,
 'HOME': None,
 'RE0': None,
 'RE1': None,
 'RE_hw_mi': None,
 'current_re': None,
 'domain': None,
 'fqdn': 'crpd-eu-fra-0',
 'hostname': 'crpd-eu-fra-0',
 'hostname_info': {'re0': 'crpd-eu-fra-0'},
 'ifd_style': 'CLASSIC',
 'junos_info': None,
 'master': None,
 'model': 'CRPD',
 'model_info': {'re0': 'CRPD'},
 'personality': None,
 're_info': None,
 're_master': None,
 'serialnumber': None,
 'srx_cluster': None,
 'srx_cluster_id': None,
 'srx_cluster_redundancy_group': None,
 'switch_style': 'NONE',
 'vc_capable': False,
 'vc_fabric': None,
 'vc_master': None,
 'vc_mode': None,
 'version': '0.0I0.0',
 'version_RE0': None,
 'version_RE1': None,
 'version_info': junos.version_info(major=(0, 0), type=I, minor=0, build=0),
 'virtual': None}

routing table:
10.128.0.0/20 Direct via eth0
10.128.0.2/32 Local via eth0
172.17.0.1/32 Local via None
185.85.196.12/32 Direct via lo.0
fe80::858:aff:fe80:2/128 Local via eth0
fe80::3c81:71ff:fef4:9b9c/128 Local via lsi
ff02::2/128 INET6 via None
```



the cRPD VM has the loopback set to the anycast IP:

lo.0             Up    MPLS  enabled
                       ISO   enabled
                       INET  185.85.196.16


```
mwiget@xeon:~/crpd-xdp-router/sp/reflector$ terraform refresh                                                                                                            
stackpath_compute_workload.reflector: Refreshing state... [id=41712f7e-ef31-447d-99eb-df2b7688f9f5]                                                                      
                                                                                                                                                                         
Outputs:                                                                                                                                                                 
                                                                                                                                                                         
anycast_ip = 185.85.196.16                                                                                                                                               
instance_ips = [                                                                                                                                                         
  [                                                                                                                                                                      
    "151.139.87.63",                                                                                                                                                     
    "151.139.87.64",                                                                                                                                                     
  ],                                                                                                                                                                     
]                                                                                                                                                                        
instances = {                                                                                                                                                            
  "reflector-eu-fra-0" = {                                                                                                                                               
    "name" = "reflector-eu-fra-0"                                                                                                                                        
    "phase" = "RUNNING"                                                                                                                                                  
    "private_ip" = "10.128.0.2"                                                                                                                                          
    "public_ip" = "151.139.87.63"                                                                                                                                        
  }                                                                                                                                                                      
  "reflector-eu-fra-1" = {                                                                                                                                               
    "name" = "reflector-eu-fra-1"                                                                                                                                        
    "phase" = "RUNNING"                                                                                                                                                  
    "private_ip" = "10.128.0.3"                                                                                                                                          
    "public_ip" = "151.139.87.64"                                                                                                                                        
  }                                                                                                                                                                      
}                                 
```

Once the clients are up and running too, they share their routes via BGP:

```
iget@xeon:~/crpd-xdp-router/sp/reflector$ ./generate-hosts-file.sh 
185.85.196.21  reflector
151.139.87.1    reflector-eu-fra-0
151.139.87.5    reflector-eu-fra-1
mwiget@xeon:~/crpd-xdp-router/sp/reflector$ ssh 151.139.87.1
Welcome to Ubuntu 18.04.4 LTS (GNU/Linux 5.3.0-59-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Wed Jun 10 14:55:22 UTC 2020

  System load:  0.0                Processes:              145
  Usage of /:   17.5% of 28.76GB   Users logged in:        1
  Memory usage: 34%                IP address for eth0:    10.128.0.2
  Swap usage:   0%                 IP address for docker0: 172.17.0.1

 * MicroK8s gets a native Windows installer and command-line integration.

     https://ubuntu.com/blog/microk8s-installers-windows-and-macos

0 packages can be updated.
0 updates are security updates.

Your Hardware Enablement Stack (HWE) is supported until April 2023.

Last login: Wed Jun 10 13:28:02 2020 from 212.51.142.174
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

mwiget@reflector-eu-fra-0:~$ docker ps
CONTAINER ID        IMAGE                    COMMAND                  CREATED             STATUS              PORTS               NAMES
48412a12853b        crpd:20.1R1.11           "/sbin/runit-init.sh"    2 hours ago         Up 2 hours                              crpd
0de5934e0955        marcelwiget/xdp_router   "/bin/sh /entrypoint…"   2 hours ago         Up 2 hours                              xdp_router
mwiget@reflector-eu-fra-0:~$ docker exec -ti crpd cli
root@reflector-eu-fra-0> show bgp summary 
Threading mode: BGP I/O
Groups: 1 Peers: 5 Down peers: 0
Unconfigured peers: 5
Table          Tot Paths  Act Paths Suppressed    History Damp State    Pending
inet.0               
                  327690     131073          0          0          0          0
inet6.0              
                       0          0          0          0          0          0
Peer                     AS      InPkt     OutPkt    OutQ   Flaps Last Up/Dwn State|#Active/Received/Accepted/Damped...
10.128.32.2      4259905000        279        190       0       0     1:24:43 Establ
  inet.0: 0/65538/65538/0
  inet6.0: 0/0/0/0
10.128.32.3      4259905000        276        187       0       0     1:23:29 Establ
  inet.0: 0/65538/65538/0
  inet6.0: 0/0/0/0
10.128.32.4      4259905000        271        183       0       0     1:21:44 Establ
  inet.0: 0/65538/65538/0
  inet6.0: 0/0/0/0
151.139.87.8     4259905000        180         97       0       0       42:42 Establ
  inet.0: 65537/65538/65538/0
  inet6.0: 0/0/0/0
151.139.87.9     4259905000        354        263       0       0     1:58:35 Establ
  inet.0: 65536/65538/65538/0
  inet6.0: 0/0/0/0

root@reflector-eu-fra-0> show route summary 
Autonomous system number: 4259905000
Router ID: 10.0.0.0

RIB Unique destination routes high watermark: 196617 at 2020-06-10 13:33:58
RIB routes high watermark: 327697 at 2020-06-10 13:33:58
FIB routes high watermark: 196611 at 2020-06-10 12:59:27
VRF type routing instances high watermark: 0 at 1970-01-01 00:00:00

inet.0: 196614 destinations, 327694 routes (131077 active, 0 holddown, 196614 hidden)
              Direct:      2 routes,      2 active
               Local:      2 routes,      2 active
                 BGP: 327690 routes, 131073 active

inet6.0: 3 destinations, 3 routes (3 active, 0 holddown, 0 hidden)
               Local:      2 routes,      2 active
               INET6:      1 routes,      1 active

root@reflector-eu-fra-0> 
```

```
$ ssh 151.139.87.31 docker exec crpd cli show bgp summ
ary
Threading mode: BGP I/O
Groups: 1 Peers: 3 Down peers: 0                                                                
Unconfigured peers: 3                                                                           
Table          Tot Paths  Act Paths Suppressed    History Damp State    Pending                 
inet.0                                                                                          
                  196614     196609          0          0          0          0                 
inet6.0                                                                                         
                       0          0          0          0          0          0                 
Peer                     AS      InPkt     OutPkt    OutQ   Flaps Last Up/Dwn State|#Active/Received/Accepted/Damped...
10.128.0.4       4259905000        124         38       0       0       15:20 Establ            
  inet.0: 65537/65538/65538/0                                                                   
  inet6.0: 0/0/0/0                                                                              
10.128.0.5       4259905000        124         36       0       0       15:20 Establ            
  inet.0: 65536/65538/65538/0                                                                   
  inet6.0: 0/0/0/0                                                                              
10.128.0.6       4259905000        124         36       0       0       15:20 Establ            
  inet.0: 65536/65538/65538/0                                                                   
  inet6.0: 0/0/0/0                                                                         
```

## Notes

- As there is no IPv6 currently within the VM, maybe force apt to use v4 only:
  `apt-get -o Acquire::ForceIPv4=true ....`



