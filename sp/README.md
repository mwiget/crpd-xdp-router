# cRPD on SP// demo

Terraform based demo of BGP between cRPD instances to route GRE over UDP traffic
between multiple clients. Tunnel endpoint can be an anycast IP address.

```mermaid
graph TB

subgraph C["C1 .. Cn"]
  crpd
  trafgen
end

subgraph Reflector2
  R2crpd
  R2xdp
end

subgraph Reflector1
  R1crpd
  R1xdp
end

crpd -.-|BGP| R1crpd
crpd -.-|BGP| R2crpd
trafgen ---|GRE| R1xdp
trafgen ---|GRE| R2xdp
```

The reflectors uses cRPD to accept internal BGP sessions for AS 65000.65000 from clients and 
updates the local routing table with learned routes. An XDP router is installed on
eth0 that does route lookups on the inner IP header of incoming GRE over UDP packets
and routes or drops them accordingly. All other incoming traffic is passed on to
the linux kernel for processing.

Clients use cRPD to generate 64k unique host routes in the 10.<client-id>.x.y/16 range and
announce them via BGP to both reflectors. The clients generate low volume GRE over UDP 
packets (roughly 1pps) with random destination in the range of announced routes towards
the given GRE tunnel endpoint IP address (typically the anycast IP of the reflector 
workload). These packets are then routed back to each client. 

Check reflector and client folder for more information.


Build and deploy first reflector routers from folder [reflector](reflector), then
note their internal IP addresses to be used as BGP endpoints plus the anycast IP 
to send tunnel traffic to. These IP addresses are then to be adjusted in the [client](client/)
 folder client.tf file. See instructions in either folder.

