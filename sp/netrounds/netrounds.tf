variable "stackpath_stack" {}
variable "stackpath_client_id" {}
variable "stackpath_client_secret" {}
variable "ncc_host" {}
variable "account" {}
variable "email" {}
variable "password" {}

provider "stackpath" {
  stack_id      = var.stackpath_stack
  client_id     = var.stackpath_client_id
  client_secret = var.stackpath_client_secret
}

resource "stackpath_compute_workload" "netrounds" {
  name = "netrounds"
  slug = "netrounds"

  network_interface {
   network = "default"
  }

#  image_pull_credentials {
#    docker_registry {
#      username = var.credentials_username
#      password = var.credentials_password
#    }
#  }

  container {

    name = "r"
    image = "marcelwiget/nr-test-agent:latest"

    resources {
      requests = {
        "cpu" = "1"
        "memory" = "2Gi"
      }
    }

    env {
      key   = "HOST"
      value = var.ncc_host
    }
    env {
      key   = "ACCOUNT"
      value = var.account
    }
    env {
      key   = "EMAIL"
      value = var.email
    }
    env {
      key   = "PASSWORD"
      value = var.password
    }

    port {
      name = "test-tcp"
      port = 5000
      protocol = "TCP"
      enable_implicit_network_policy = true
    }
    port {
      name = "test-udp"
      port = 5000
      protocol = "UDP"
      enable_implicit_network_policy = true
    }
  }

  target {
    name = "eu"
    min_replicas = 1
    max_replicas = 1
    deployment_scope = "cityCode"
    selector {
      key      = "cityCode"
      operator = "in"
      values   = [
        "AMS", "FRA", "LHR", "WAW"
      ]
    }
  }
}

output "instances" {
  value = {
    for instance in stackpath_compute_workload.netrounds.instances :
    instance.name => {
      "name"       = instance.name
      "public_ip"  = instance.external_ip_address
      "private_ip" = instance.ip_address
      "phase"      = instance.phase
    }
  }
}

output "instance_ips" {
  value = ["${stackpath_compute_workload.netrounds.instances.*.external_ip_address}"]
}
