#!/bin/bash
NAME=$(hostname)
set -e
/netrounds-test-agent-application/netrounds-test-agent-application register --config=/config.json --log-level=DEBUG --ssl-no-check --ncc-host=$HOST --account=$ACCOUNT --email=$EMAIL --password=$PASSWORD --name=$NAME --config=/config.json
/netrounds-test-agent-application/netrounds-test-agent-application --config=/config.json --log-level=DEBUG
