#!/bin/bash
tfvars=terraform.tfvars
ncc_host=$(grep ncc_host $tfvars| cut -d\" -f2)
email=$(grep email $tfvars| cut -d\" -f2)
account=$(grep account $tfvars| cut -d\" -f2)
password=$(grep password $tfvars| cut -d\" -f2)

docker run -t --rm -d --net host --name nr-test-agent \
  --env HOST=$ncc_host \
  --env ACCOUNT=$account \
  --env EMAIL=$email \
  --env PASSWORD=$password \
  marcelwiget/nr-test-agent:latest
