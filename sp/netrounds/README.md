# netrounds test agents in SP//

Adjust secrets for SP// and netrounds in terraform.tfvars:

```
stackpath_stack = ""
stackpath_client_id = ""
stackpath_client_secret = ""
ncc_host = ""
account = ""
email = ""
password = ""
```

Build the test agent container so it can auto-register. Push the 
container to a container registry reachable by SP//. Adjust name
accordingly in Makefile:

```
$ make build
# make push
```

Initialize and deploy via terraform:

```
$ terraform init
$ terraform apply
```

Verify all agents are running:

```
netrounds$ terraform refresh
stackpath_compute_workload.netrounds: Refreshing state... [id=a2aba8d8-f26d-4413-a57f-b07f14646038]
                                                                                    
Outputs:                                                                            
                                                                                    
instance_ips = [                                                                    
  [                                                                                 
    "151.139.83.99",                                                                
    "151.139.43.97",                                                                
    "151.139.87.62",                                                                
    "151.139.186.26",                                                               
  ],                                                                                
]                                                                                   
instances = {                                                                       
  "netrounds-eu-ams-0" = {                                                          
    "name" = "netrounds-eu-ams-0"                                                   
    "phase" = "RUNNING"                                                             
    "private_ip" = "10.128.0.2"                                                     
    "public_ip" = "151.139.83.99"                                                   
  }                                                                                 
  "netrounds-eu-fra-0" = {                                                          
    "name" = "netrounds-eu-fra-0"                                                   
    "phase" = "RUNNING"                                                             
    "private_ip" = "10.128.16.2"                                                    
    "public_ip" = "151.139.87.62"                                                   
  }                                                                                 
  "netrounds-eu-lhr-0" = {                                                          
    "name" = "netrounds-eu-lhr-0"                                                   
    "phase" = "RUNNING"                                                             
    "private_ip" = "10.128.32.2"                                                    
    "public_ip" = "151.139.43.97"                                                   
  }                                                                                 
  "netrounds-eu-waw-0" = {                                                          
    "name" = "netrounds-eu-waw-0"                                                   
    "phase" = "RUNNING"                                                             
    "private_ip" = "10.128.48.2"                                                    
    "public_ip" = "151.139.186.26"                                                  
  }                                                                                 
}                                                  
